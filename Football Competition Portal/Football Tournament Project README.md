Football Tournament Project
Oduniyi Ibukunoluwa (1837597) Section 1 
This project tracks and manages football games in a Fictitious World Cup Competition. This competition is modelled to work exactly how a world cup competition does. In a nutshell, teams are grouped, and matches are generated that ensures each team in the groups faceoff against each other with the two top teams progressing to the knockout phase  which in itself comprises of several versus-elimination matches until an eventual winner is crowned. 

Probable Use cases:
1.	Players registers for fixtures
2.	Players can get a list of Fixtures their Team is scheduled for or have partaken in
3.	Players notify coaches of their availability 
4.	Coaches get Messages from Players
5.	Coaches see a list of Fixtures for their respective Teams
6.	Coaches can view Players who are registered for an upcoming Fixture
7.	Coaches can select Players to play in a particular Fixture
8.	Admin is able to control the entire system
9.	Admin can add Fixtures
10.	Admin is responsible for updating fixtures with events
11.	Admin can view all Fixtures
12.	Admin as access to the entire tournament Information 
13.	Admin is Responsible for entering Stadium Information into the database. 
