<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	   <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<script>
	function ajaxEvent() {
		var xmlHttp;
		try // Firefox, Opera 8.0+, Safari
		{
			xmlHttp = new XMLHttpRequest();
		} catch (e) {
			try // Internet Explorer
			{
				xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try {
					xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {
					alert("Your browser does not support AJAX!");
					return false;
				}
			}
		}

		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4) {
				document.getElementById("coursediv").innerHTML = xmlHttp.responseText;
			}
		}

		var queryString = document.getElementById("queryString").value;
		xmlHttp.open("POST", "/finalproject/ajaxservice.htm?teamname="
				+ queryString, true);
		xmlHttp.send();
	}
</script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Dashboard</title>
<style>
         table, th , td {
            border: 1px solid grey;
            border-collapse: collapse;
            padding: 5px;
         }
         
         table tr:nth-child(odd) {
            background-color: #f2f2f2;
         }
         
         table tr:nth-child(even) {
            background-color: #ffffff;
         }
      </style>

</head>
<body>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<a href="${contextPath}/user/logout">[Log Out]</a>
	<h5>Search Teams:</h5>
<form action="${contextPath}/user/searchteam" method="post">
		<table>
			<tbody>
				<td><input type="text" name="team" id="queryString" size="30"
					onkeyup="ajaxEvent()" /></td>
				<td><input type="submit" value="search" /></td>
			</tbody>
			</table>
			</form>
			<div id="coursediv"></div>
	<h2>Player dashboard: ${sessionScope.user.person.firstName} ${sessionSscope.user.person.lastName}</h2>


<div>
		<h4>Played Fixtures</h4>
	<table>
		<thead>
			<th>Date</th>
			<th>Team 1</th>
			<th>Team 2</th>
			<th>Match Type</th>
			<th>Stadium</th>
			<th>Referee</th>
		
		</thead>
		<tbody>
			<c:forEach items="${fixtures}" var="fixture">
			<c:if test="${fixture.status eq 'Played' }">
				<tr>
					<td>${fixture.fixtureDate}</td>
					<td>${fixture.team1.teamName}</td>
					<td>${fixture.team2.teamName}</td>
					<td>${fixture.matchType}</td>
					<td>${fixture.stadium.stadiumName}</td>
					<td>${fixture.ref.firstName}
						${fixture.ref.lastName}</td>
				</tr>
				</c:if>
			</c:forEach>
		</tbody>
	</table>
	</p>
	</div>

	<div>
		<h4>Upcoming Fixtures</h4>
	<table>
		<thead>
			<th>Date</th>
			<th>Team 1</th>
			<th>Team 2</th>
			<th>Match Type</th>
			<th>Stadium</th>
			<th>Referee</th>
			<th></th>
		</thead>
		<tbody>
			<c:forEach items="${fixtures}" var="fixture">
			<c:if test="${fixture.status eq 'Not Played' }">
				<tr>
					<td>${fixture.fixtureDate}</td>
					<td>${fixture.team1.teamName}</td>
					<td>${fixture.team2.teamName}</td>
					<td>${fixture.matchType}</td>
					<td>${fixture.stadium.stadiumName}</td>
					<td>${fixture.ref.firstName}
						${fixture.ref.lastName}</td>
						<td><a href="${contextPath}/player/register?fixture=${fixture.matchID}&player=${sessionScope.user.person.personnelID}"> Register </a></td>
				</tr>
				</c:if>
			</c:forEach>
		</tbody>
	</table>
	</p>
	</div>
	
	<div>
		<h4>Fixtures You Are Registered For</h4>
	<table>
		<thead>
			<th>Date</th>
			<th>Team 1</th>
			<th>Team 2</th>
			<th>Match Type</th>
			<th>Stadium</th>
			<th>Referee</th>
			<th></th>
		</thead>
		<tbody>
			<c:forEach items="${player.fixtures}" var="fixtures">
			<c:if test="${fixtures.status eq 'Not Played' }">
				<tr>
					<td>${fixtures.fixtureDate}</td>
					<td>${fixtures.team1.teamName}</td>
					<td>${fixtures.team2.teamName}</td>
					<td>${fixtures.matchType}</td>
					<td>${fixtures.stadium.stadiumName}</td>
					<td>${fixtures.ref.firstName}
						${fixtures.ref.lastName}</td>
						<td><a href="${contextPath}/player/unregister?fixture=${fixtures.matchID}&player=${sessionScope.user.person.personnelID}"> unregister </a></td>
				</tr>
				</c:if>
			</c:forEach>
		</tbody>
	</table>
	</p>
	</div>
	
	<div>
		<h4> Fixtures You're Selected For</h4>
	<table>
		<thead>
			<th>Date</th>
			<th>Team 1</th>
			<th>Team 2</th>
			<th>Match Type</th>
			<th>Stadium</th>
			<th>Referee</th>
			<th></th>
		</thead>
		<tbody>
			<c:forEach items="${player.selectedFixtures}" var="selectedfixtures">
			<c:if test="${selectedfixtures.status eq 'Not Played' }">
				<tr>
					<td>${selectedfixtures.fixtureDate}</td>
					<td>${selectedfixtures.team1.teamName}</td>
					<td>${selectedfixtures.team2.teamName}</td>
					<td>${selectedfixtures.matchType}</td>
					<td>${selectedfixtures.stadium.stadiumName}</td>
					<td>${selectedfixtures.ref.firstName}
						${selectedfixtures.ref.lastName}</td>
						<td></td>
				</tr>
				</c:if>
			</c:forEach>
		</tbody>
	</table>
	</p>
	</div>
</body>
</html>