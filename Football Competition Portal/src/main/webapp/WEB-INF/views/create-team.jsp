<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Create Team</title>
</head>
<body>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<a href="${contextPath}/admin/home">[Home]</a>
	<form action="${contextPath}/admin/createteam" method="POST">
		<table>
			<tr>
				<td>Team Name:</td>
				<td><input type="text" name="teamname" required="required" /></td>
			</tr>

			<tr>
				<td>group:</td>
				<td><input type="text" name="group" size="1"
					required="required" /></td>
			</tr>
			

			<tr>
				<td colspan="2"><input type="submit" value="submit" /></td>
			</tr>

		</table>
	</form>
</body>
</html>