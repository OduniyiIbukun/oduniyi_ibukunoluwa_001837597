	<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    	   <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Select Players</title>
</head>
<body>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<a href="${contextPath}/user/logout">[Log Out]</a>
<div>
		<h4>Fixture</h4>
	<table>
		<thead>
			<th>Date</th>
			<th>Team 1</th>
			<th>Team 2</th>
			<th>Match Type</th>
			<th>Stadium</th>
			<th>Referee</th>
			
		</thead>
		<tbody>
			
				<tr>
					<td>${fixture.fixtureDate}</td>
					<td>${fixture.team1.teamName}</td>
					<td>${fixture.team2.teamName}</td>
					<td>${fixture.matchType}</td>
					<td>${fixture.stadium.stadiumName}</td>
					<td>${fixture.ref.firstName}
						${fixture.ref.lastName}</td>
				</tr>
		</tbody>
	</table>
	</p>
	</div>
	
	<h2> Registered Players </h2>
<table>
		<thead>
			<th>player Name</th>
			<th> Squad Number</th>
			<th>Position</th>
			<th></th>
		</thead>
		<tbody>
			<c:forEach items="${registeredplayers}" var="player">
				<tr>
					<td>${player.firstName} ${player.lastName}</td>
					<td>${player.squadNumber}</td>
					<td>${player.position}</td>
						<td><a href="${contextPath}/coach/select?fixture=${fixture.matchID}&player=${player.personnelID}&team=${team}"> Select For Fixture </a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	
  <c:if test="${not empty selectedplayers}">
  
  <h2> Selected Players </h2>
<table>
		<thead>
			<th>player Name</th>
			<th> Squad Number</th>
			<th>Position</th>
			<th></th>
		</thead>
		<tbody>
			<c:forEach items="${selectedplayers}" var="player">
				<tr>
					<td>${player.firstName} ${player.lastName}</td>
					<td>${player.squadNumber}</td>
					<td>${player.position}</td>
						<td><a href="${contextPath}/coach/unselect?fixture=${fixture.matchID}&player=${player.personnelID}&team=${team}"> Unselect For Fixture </a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
  </c:if>
</body>
</html>