<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>select an event</title>

</head>
<body>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<a href="${contextPath}/admin/home">[Home]</a>
	<font color="red">${errorMessage}</font>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<form action="${contextPath}/admin/selectevent" method="POST">
		<table>
			<tr>
				<td>Category:</td>
				<td><select name="category">
						<option value="goal">Goal</option>
						<option value="substitution">Substitution</option>
						<option value="discipline">Discipline</option>
				</select></td>
			</tr>

			<tr>
				<td>Team:</td>
				<td><select name="team">
				
						<option value="${team1}">${team1}</option>
						<option value="${team2}">${team2}</option>
				</select></td>
			</tr>

			<tr>
				<td colspan="2"><input type="submit" value="submit" /></td>
			</tr>

			<input type="hidden" value="${fixture}" name="fixture">
		</table>
	</form>
</body>
</html>