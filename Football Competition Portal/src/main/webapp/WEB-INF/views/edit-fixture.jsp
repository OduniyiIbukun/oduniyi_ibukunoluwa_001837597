<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${fixture.team1.teamName}vs${fixture.team1.teamName}</title>
</head>
<body>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<a href="${contextPath}/admin/home">[Home]</a>
	<font color="red">${errorMessage}</font>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<h3>Fixture</h3>
	<table>
		<thead>
			<th>Date</th>
			<th>Team 1</th>
			<th>Team 2</th>
			<th>Match Type</th>
			<th>Status</th>
			<th>Stadium</th>
			<th>Referee</th>
			<th></th>
			<th></th>
		</thead>
		<tbody>
			<tr>
				<td>${fixture.fixtureDate}</td>
				<td>${fixture.team1.teamName}</td>
				<td>${fixture.team2.teamName}</td>
				<td>${fixture.matchType}</td>
				<td>${fixture.status}</td>
				<td>${fixture.stadium.stadiumName}</td>
				<td>${fixture.ref.firstName}${fixture.ref.lastName}</td>

			</tr>

			<c:if test="${not empty teams}">
				<form action="${contextPath}/admin/update" method="POST">
					<table>
						<tr>
							<td>Date:</td>
							<td><input type="date" name="date" required="required"
								value=${fixture.fixtureDate} placeholder="${fixture.fixtureDate}" /></td>
						</tr>

						<tr>
							<td>Team1</td>
							<td><select name="team1" required="required"
								placeholder="${fixture.team1.teamName}">
									<c:forEach items="${teams}" var="team">
										<option value="${team.teamName}">${team.teamName}</option>
									</c:forEach>
							</select></td>
						</tr>


						<tr>
							<td>Team2:</td>
							<td><select name="team2" required="required"
								placeholder="${fixture.team2.teamName}">
									<c:forEach items="${teams}" var="team">
										<option value="${team.teamName}">${team.teamName}</option>
									</c:forEach>
							</select></td>
						</tr>

						<tr>
							<td>Status:</td>
							<td><select name="status" required="required" placeholder="${fixture.status}">

									<option value="Not Played">Not Played</option>
									<option value="Played">Played</option>
							</select></td>
						</tr>

						<tr>
							<td>Stadium:</td>
							<td><select name="stadium">
									<c:forEach items="${stadiums}" var="stadium">
										<option value="${stadium.stadiumName}">${stadium.stadiumName}</option>
									</c:forEach>
							</select></td>
						</tr>

						<tr>
							<td>Attendance</td>
							<td><input type="text" name="attendance"></td>
						</tr>

						<tr>
							<td>Referee:</td>
							<td><select name="referee">
									<c:forEach items="${referees}" var="referee">
										<option value="${referee.personnelID}">${referee.firstName} ${referee.lastName}</option>
									</c:forEach>
							</select></td>
						</tr>

						<input type="hidden" name="fixture" value="${fixtureID}" />

						<tr>
							<td colspan="2"><input type="submit" value="submit" /></td>
						</tr>

					</table>
				</form>
			</c:if>
</body>
</html>