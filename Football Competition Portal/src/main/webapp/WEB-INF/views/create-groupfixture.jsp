<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<a href="${contextPath}/admin/home">[Home]</a>
	<h2>Create Group Fixture</h2>


	<form action="${contextPath}/admin/selectgroup.htm" method="POST">
		<h6>select a group below</h6>
		<select name="group" required="required">
			<option value="A">A</option>
			<option value="B">B</option>
			<option value="C">C</option>
			<option value="D">D</option>
			<option value="E">E</option>
			<option value="F">F</option>
			<option value="G">G</option>
			<option value="H">H</option>
			<option value="I">I</option>
			<option value="J">J</option>
		</select>
		</td> <input type="submit" value="submit" />
	</form>

	<c:if test="${not empty teams}">
		<font color="red">${errorMessage}</font>
		<c:set var="contextPath" value="${pageContext.request.contextPath}" />
		<form action="${contextPath}/admin/createfixture" method="POST">
			<table>
				<tr>
					<td>Date:</td>
					<td><input type="date" name="date" required="required" /></td>
				</tr>

				<tr>
					<td>Team1</td>
					<td><select name="team1" required="required">
							<c:forEach items="${teams}" var="team">
								<option value="${team.teamName}">${team.teamName}</option>
							</c:forEach>
					</select></td>
				</tr>

				<tr>
					<td>Team2:</td>
					<td><select name="team2" required="required">
							<c:forEach items="${teams}" var="team">
								<option value="${team.teamName}">${team.teamName}</option>
							</c:forEach>
					</select></td>
				</tr>

				<tr>
					<td>Stadium:</td>
					<td><select name="stadium">
							<c:forEach items="${stadiums}" var="stadium">
								<option value="${stadium.stadiumName}">${stadium.stadiumName}</option>
							</c:forEach>
					</select></td>
				</tr>

				<tr>
					<td>Referee:</td>
					<td><select name="referee">
							<c:forEach items="${referees}" var="referee">
								<option value="${referee.personnelID}">${referee.firstName}</option>
							</c:forEach>
					</select></td>
				</tr>

				<input type="hidden" name="category" value="Group" />
				<input type = "hidden" name="group" value="${group}">
				
				<tr>
					<td colspan="2"><input type="submit" value="submit" /></td>
				</tr>

			</table>
		</form>
	</c:if>
</body>
</html>