<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add a goal to this Fixture</title>
 <style>
         table, th , td {
            border: 1px solid grey;
            border-collapse: collapse;
            padding: 5px;
         }
         
         table tr:nth-child(odd) {
            background-color: #f2f2f2;
         }
         
         table tr:nth-child(even) {
            background-color: #ffffff;
         }
      </style>
</head>
<body>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<a href="${contextPath}/admin/home">[Home]</a>
<h1> Team: ${team.teamName}</h1>
<h4> Add a Goal </h4>
<font color="red">${errorMessage}</font>
	
	<form action="${contextPath}/admin/addgoal" method="POST">
		<table>
		
			<tr>
				<td>Player:</td>
				<td><select name="player" required="required">
				<c:forEach items="${selectedplayers}" var="player">
						<option value="${player.personnelID}">${player.firstName} ${player.lastName}</option>
						</c:forEach>
				</select></td>
			</tr>

			<tr>
				<td>Time:</td>
				<td><select name="time" required="required">
						<c:forEach begin="1" end="120" varStatus="loop" >
							<option>${loop.index} </option>
						</c:forEach>
				</select></td>
			</tr>	
			<input type="hidden" value="${team.teamName}" name="team">
			<input type="hidden" value="${fixture}" name="fixture">
			<tr>
				<td colspan="2"><input type="submit" value="Add" /></td>
			</tr>

		</table>
	</form>
	
	<div>
		<h4>Goals for this fixture</h4>
	<table>
		<thead>
			<th>Player </th>
			<th>time</th>
		
		</thead>
		<tbody>
			<c:forEach items="${goalevents}" var="goalevent">
				<tr>
					<td>${goalevent.player.firstName} ${goalevent.player.lastName}</td>
					<td>${goalevent.eventTime}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</p>
	</div>
	
</body>
</html>