<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    	   <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<style>
         table, th , td {
            border: 1px solid grey;
            border-collapse: collapse;
            padding: 5px;
         }
         
         table tr:nth-child(odd) {
            background-color: #f2f2f2;
         }
         
         table tr:nth-child(even) {
            background-color: #ffffff;
         }
      </style>

</head>
<body>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<a href="${contextPath}/user/logout">[Log Out]</a>
<h2>Coach DashBoard: ${sessionScope.user.person.firstName} ${sessionSscope.user.person.lastName}</h2>
<h4> Team: ${team.teamName}</h4>
	
	<!-- Search Courses:
	<input type="text" id="queryString" size="30" onkeyup="ajaxEvent()" />

	<div id="coursediv"></div> -->

	<div>
		<h4>Upcoming Fixtures</h4>
	<table>
		<thead>
			<th>Date</th>
			<th>Team 1</th>
			<th>Team 2</th>
			<th>Match Type</th>
			<th>Stadium</th>
			<th>Referee</th>
			<th></th>
		</thead>
		<tbody>
			<c:forEach items="${fixtures}" var="fixture">
			<c:if test="${fixture.status eq 'Not Played' }">
				<tr>
					<td>${fixture.fixtureDate}</td>
					<td>${fixture.team1.teamName}</td>
					<td>${fixture.team2.teamName}</td>
					<td>${fixture.matchType}</td>
					<td>${fixture.stadium.stadiumName}</td>
					<td>${fixture.ref.firstName}
						${fixture.ref.lastName}</td>
						<td><a href="${contextPath}/coach/viewregisteredplayers?fixture=${fixture.matchID}&team=${team.teamName}"> Select Squad </a></td>
				</tr>
				</c:if>
			</c:forEach>
		</tbody>
	</table>
	</p>
	</div>
	
	<div>
		<h4>New Messages</h4>
	<table border = 1>
		<thead>
			<th>Date</th>
			<th>From</th>
			<th>Action</th>
		</thead>
		<tbody>
			<c:forEach items="${messages}" var="message">
			<c:if test="${message.status eq 'Unread' }">
				<tr>
					<td>${message.messageDate}</td>
					<td>${message.sender.person.firstName} ${message.sender.person.lastName}</td>
					<td rowspan="2"><a href="${contextPath}/coach/readmessage?message=${message.messageID}"> Mark as read </a></td>
				</tr>
				<tr>
				<td colspan = "2">${message.content} </td>
				</tr>
				</c:if>
			</c:forEach>
		</tbody>
	</table>
	</p>
	</div>
	
</body>
</html>