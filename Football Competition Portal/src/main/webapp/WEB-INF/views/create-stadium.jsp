<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
       <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<a href="${contextPath}/admin/home">[Home]</a>
<h2> Create A New Stadium </h2>
<font color="red">${errorMessage}</font>
	
	<form action="${contextPath}/admin/createstadium.htm" method="POST">
		<table>
			<tr>
				<td>Stadium Name:</td>
				<td><input type="text" name="name" size="50"
					required="required" /></td>
			</tr>

			<tr>
				<td>Stadium Location:</td>
				<td><input type="name" name="location" size="50"
					required="required" /></td>
			</tr>

			<tr>
				<td>Capacity:</td>
				<td><input type="text" name="capacity" size="30"
					required="required" /></td>
			</tr>

			<tr>
				<td colspan="2"><input type="submit" value="submit" /></td>
			</tr>

		</table>
	</form>
	
	<c:if test="${empty stadiums}"> No Stadiums Added yet</c:if>
	<c:if test="${not empty stadiums}">
	<table>
	<thead>
	<th> Stadium Name </th>
	<th> Stadium Location </th>
	<th> Stadium Capacity </th>
	</thead>
	<tbody>
	<c:forEach items="${stadiums}" var="stadium">
	<tr>
	<td>${stadium.stadiumName}</td>
	<td>${stadium.stadiumLocation}</td>
	<td>${stadium.capacity}</td>
	</tr>
	</c:forEach>
	</tbody>
	</table>
	</c:if>
</body>
</html>