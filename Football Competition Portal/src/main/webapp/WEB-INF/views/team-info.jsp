<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<style>
         table, th , td {
            border: 1px solid grey;
            border-collapse: collapse;
            padding: 5px;
         }
         
         table tr:nth-child(odd) {
            background-color: #f2f2f2;
         }
         
         table tr:nth-child(even) {
            background-color: #ffffff;
         }
      </style>
      
</head>
<body>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<a href="${contextPath}/admin/home">[Home]</a>
	<h2>Coach</h2>
	<div>
		<table border=1>
			<thead>
				<th>Coach Name</th>
				<th>Country</th>
			</thead>
			<tbody>
			
					<tr>
						<td>${coach.firstName} ${coach.lastName}</td>
						<td>${team}</td>
					</tr>
			</tbody>
		</table>
		</p>
	</div>

	<div>
		<h2>Squad Details</h2>
		<table border=1>
			<thead>
				<th>Full Name</th>
				<th>Squad Number</th>
				<th>Country</th>
			</thead>
			<tbody>
				<c:forEach items="${squaddetails}" var="player">
					<tr>
						<td>${player.firstName} ${player.lastName}</td>
						<td>${player.squadNumber}</td>
						<td>${player.team.teamName}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</p>
	</div>

	<div>
		<h2>All Fixtures for ${team}</h2>
		<table border = 2>
			<thead>
				<th>Match Stage</th>
				<th>Match Date</th>
				<th>Game</th>
				<th>Stadium Name</th>
				<th>Referee</th>
				<th>Attendance</th>
			</thead>
			<tbody>
				<c:forEach items="${fixtures}" var="fixture">
					<tr>
						<td>${fixture.matchType}</td>
						<td>${fixture.fixtureDate}</td>
						<td>${fixture.team1.teamName} vs ${fixture.team2.teamName}</td>
						<td>${fixture.stadium.stadiumName}</td>
						<td>${fixture.ref.firstName} ${fixture.ref.lastName}</td>
						<td>${fixture.attendance}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</p>
	</div>

</body>
</html>