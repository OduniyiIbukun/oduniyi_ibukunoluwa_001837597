<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<a href="${contextPath}/admin/home">[Home]</a>
	<h2>Create Knockout Fixture</h2>
	

	<c:if test="${not empty teams}">
		<font color="red">${errorMessage}</font>
		<c:set var="contextPath" value="${pageContext.request.contextPath}" />
		<form action="${contextPath}/admin/createfixture" method="POST">
			<table>
				<tr>
					<td>Date:</td>
					<td><input type="date" name="date" required="required" /></td>
				</tr>

				<tr>
					<td>Team1</td>
					<td><select name="team1" required="required">
							<c:forEach items="${teams}" var="team">
								<option value="${team.teamName}">${team.teamName}</option>
							</c:forEach>
					</select></td>
				</tr>

				<tr>
					<td>Team2:</td>
					<td><select name="team2" required="required">
							<c:forEach items="${teams}" var="team">
								<option value="${team.teamName}">${team.teamName}</option>
							</c:forEach>
					</select></td>
				</tr>


				<tr>
					<td>Knockout Round:</td>
					<td><select name="round">

							<option value="Round Of 16">Round Of 16</option>
							<option value="Quarter-Finals">Quarter-Finals</option>
							<option value="Semi-Finals">Semi-Finals</option>
							<option value="Final">Final</option>

					</select></td>
				</tr>

				<tr>
					<td>Stadium:</td>
					<td><select name="stadium">
							<c:forEach items="${stadiums}" var="stadium">
								<option value="${stadium.stadiumName}">${stadium.stadiumName}</option>
							</c:forEach>
					</select></td>
				</tr>



				<tr>
					<td>Referee:</td>
					<td><select name="referee">
							<c:forEach items="${referees}" var="referee">
								<option value="${referee.personnelID}">${referee.firstName}</option>
							</c:forEach>
					</select></td>
				</tr>

				<input type="hidden" name="category" value="Knockout" />

				<tr>
					<td colspan="2"><input type="submit" value="submit" /></td>
				</tr>

			</table>
		</form>
	</c:if>
</body>
</html>