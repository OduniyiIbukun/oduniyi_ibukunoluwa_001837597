<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.captcha.botdetect.web.servlet.Captcha"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Registration</title>
</head>

<body>
	<%-- <c:set var="contextPath" value="${pageContext.request.contextPath}" />
<form action = "${contextPath}/selectcategory" method="POST">
	<select name = "category">
					<option value="Referee">Referee</option>
					<option value="Player">Player</option>
					<option value="Coach">Coach</option>
				</select>
				<input type="submit" value="Login" />
</form> --%>


	<font color="red">${errorMessage}</font>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<form action="${contextPath}/user/create.htm" method="POST">
		<table>
			<tr>
				<td>User Email:</td>
				<td><input type="text" name="username" size="30"
					required="required" /></td>
			</tr>

			<tr>
				<td>Password:</td>
				<td><input type="password" name="password" size="30"
					required="required" /></td>
			</tr>

			<tr>
				<td>First Name:</td>
				<td><input type="text" name="firstname" size="30"
					required="required" /></td>
			</tr>

			<tr>
				<td>Last Name:</td>
				<td><input type="text" name="lastname" size="30"
					required="required" /></td>
			</tr>

			<tr>
				<td>Date Of Birth:</td>
				<td><input type="date" name="DOB" required="required" /></td>
			</tr>


			<tr>
				<td>Squad Number:</td>
				<td><input type="text" name="squadnumber" required="required" /></td>
			</tr>

			<tr>
				<td>Position:</td>
				<td><select name="position" required="required">
						<option value="Attacker">Attacker</option>
						<option value="Midfielder">Midfielder</option>
						<option value="Defender">Defender</option>
						<option value="Goalkeeper">Goalkeeper</option>
				</select></td>
			</tr>

			<tr>
				<td>Team:</td>
				<td><select name="team" required="required">
						<c:forEach items="${teams}" var="team">
							<option value="${team.teamName}">${team.teamName}</option>
						</c:forEach>
				</select></td>
			</tr>

			<tr>
				<td colspan="2"><label for="captchaCode" class="prompt">Retype
						the characters from the picture:</label> <!-- Captcha code --> <%
				Captcha captcha = Captcha.load(request, "CaptchaObject");
				captcha.setUserInputID("captchaCode");
				
				String captchaHtml = captcha.getHtml();
				out.println(captchaHtml);
				%> <input id="captchaCode" type="text" name="captchaCode"
					required="required" /></td>
			</tr>
			
			<input type="hidden" name="category" value="${category}"/>

			<tr>
				<td colspan="2"><input type="submit" value="Login" /></td>
			</tr>

		</table>
	</form>

</body>
</html>