<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Admin Page</title>
<style>
table, th, td {
	border: 1px solid grey;
	border-collapse: collapse;
	padding: 5px;
}

table tr:nth-child(odd) {
	background-color: #f2f2f2;
}

table tr:nth-child(even) {
	background-color: #ffffff;
}
</style>



<script>
	function ajaxEvent() {
		var xmlHttp;
		try // Firefox, Opera 8.0+, Safari
		{
			xmlHttp = new XMLHttpRequest();
		} catch (e) {
			try // Internet Explorer
			{
				xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try {
					xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {
					alert("Your browser does not support AJAX!");
					return false;
				}
			}
		}

		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4) {
				document.getElementById("coursediv").innerHTML = xmlHttp.responseText;
			}
		}

		var queryString = document.getElementById("queryString").value;
		xmlHttp.open("POST", "/finalproject/ajaxservice.htm?teamname="
				+ queryString, true);
		xmlHttp.send();
	}
</script>





</head>
<body>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<a href="${contextPath}/user/logout">[Log Out]</a>

	<script
		src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular.min.js"></script>

	<script>
		function teamController($scope, $http) {
			var queryString = document.getElementById("queryString").value;
			var url = "/finalproject/admin/ajaxteams.htm?teamname="
					+ queryString;

			$http.get(url).then(function(response) {
				$scope.fixtures = response.data;
			});
		}
	</script>

	<h1>Admin Page</h1>



	<!-- <div ng-app = ""  ng-controller = "teamController">
      
         <table>
            <tr>
               <th>fixture date</th>
               <th>team1</th>
                <th>team2</th>
                 <th>matchtype</th>
                 <th>stadium</th>
               <th>Referee</th>
            </tr>

			<tr ng-repeat="fixture in fixtures">
				<td>{{fixture.fixturedate }}</td>
				<td>{{fixture.team1 }}</td>
				<td>{{fixture.team2 }}</td>
				<td>{{fixture.matchttype }}</td>
				<td>{{fixture.stadium }}</td>
				<td>{{fixture.refereefirstname }} {{fixture.refereelastname}}</td>
			</tr>
		</table>
      </div> -->


	<a href="${contextPath}/admin/createteam"> [Add a Team]</a>
	<a href="${contextPath}/admin/createfixture"> [Add a Group Fixture]</a>
	<a href="${contextPath}/admin/createknockoutfixture"> [Add a
		Knockout Fixture]</a>
	<a href="${contextPath}/admin/createstadium"> [Add a Stadium]</a>
	<a href="${contextPath}/admin/info"> [Tournament Information]</a>
	<br> Search Teams:
	<form action="${contextPath}/admin/searchteam" method="post">
		<table>
			<tbody>
				<td><input type="text" name="team" id="queryString" size="30"
					onkeyup="ajaxEvent()" /></td>
				<td><input type="submit" value="search" /></td>
			</tbody>
			</table>
			</form>
	
			<div id="coursediv"></div>

			<h4>Teams</h4>
			<table>
				<thead>
					<th>Team</th>
					<th>Group</th>
				</thead>
				<tbody>
					<c:forEach items="${teams}" var="team">
						<tr>
							<td>${team.teamName}</td>
							<td>${team.group}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

			<p>
			<h4>Past Fixtures</h4>
			<table>
				<thead>
					<th>Date</th>
					<th>Team 1</th>
					<th>Team 2</th>
					<th>Match Type</th>
					<th>Stadium</th>
					<th>Referee</th>
					<th></th>
					<th></th>
				</thead>
				<tbody>
					<c:forEach items="${fixtures}" var="fixture">
						<c:if test="${fixture.status eq 'Played' }">

							<tr>
								<td>${fixture.fixtureDate}</td>
								<td>${fixture.team1.teamName}</td>
								<td>${fixture.team2.teamName}</td>
								<td>${fixture.matchType}</td>
								<td>${fixture.stadium.stadiumName}</td>
								<td>${fixture.ref.firstName}${fixture.ref.lastName}</td>
								<td><a
									href="${contextPath}/admin/update?fixture=${fixture.matchID}">
										[Update]</a></td>
								<td><a
									href="${contextPath}/admin/addevent?fixture=${fixture.matchID}">
										[Add Event]</a></td>
							</tr>
						</c:if>
					</c:forEach>
				</tbody>
			</table>
			</p>
			<p>
			<h4>Upcoming Fixtures</h4>
			<table>
				<thead>
					<th>Date</th>
					<th>Team 1</th>
					<th>Team 2</th>
					<th>Match Type</th>
					<th>Stadium</th>
					<th>Referee</th>
					<th></th>
					<th></th>
				</thead>
				<tbody>
					<c:forEach items="${fixtures}" var="fixture">
						<c:if test="${fixture.status eq 'Not Played' }">
							<tr>
								<td>${fixture.fixtureDate}</td>
								<td>${fixture.team1.teamName}</td>
								<td>${fixture.team2.teamName}</td>
								<td>${fixture.matchType}</td>
								<td>${fixture.stadium.stadiumName}</td>
								<td>${fixture.ref.firstName}${fixture.ref.lastName}</td>
								<td><a
									href="${contextPath}/admin/update?fixture=${fixture.matchID}">
										[Edit]</a></td>
								<td><a
									href="${contextPath}/admin/addevent?fixture=${fixture.matchID}">
										[Add Event]</a></td>
							</tr>
						</c:if>
					</c:forEach>
				</tbody>
			</table>
			</p>
</body>
</html>