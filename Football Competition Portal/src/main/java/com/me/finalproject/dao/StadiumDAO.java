package com.me.finalproject.dao;

import java.util.ArrayList;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import com.me.finalproject.pojo.Stadium;
import com.me.finalproject.pojo.Team;

public class StadiumDAO extends DAO{

	public Stadium addStadium(Stadium stadium) {
		try {
			begin();
			System.out.println("inside DAO");
			getSession().save(stadium);
			commit();
			close();
			return stadium;

		} catch (HibernateException e) {
			rollback();
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	public ArrayList<Stadium> getStadiums() throws Exception {
		try {
			begin();
			Query q = getSession().createQuery("from Stadium");
			ArrayList<Stadium> stadiums = (ArrayList<Stadium>) q.list();
			close();
			return stadiums;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Could not get Stadiums " + e);
		}
	}
	
	public Stadium getStadium(String stadiumName) throws Exception{
		try {
			begin();
			System.out.println("inside DAO");
			Query q = getSession().createQuery("From Stadium where stadiumName = :stadiumName");
			q.setString("stadiumName", stadiumName);
			Stadium stadium = (Stadium) q.uniqueResult();
			close();
			return stadium;
		} catch(HibernateException e) {
			System.out.println("Error Encountered "+e.getMessage());
			rollback();
			throw new Exception("Could not get Stadium " +e);
		}
		
	}
}
