package com.me.finalproject.dao;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import com.me.finalproject.pojo.Discipline;
import com.me.finalproject.pojo.Event;
import com.me.finalproject.pojo.Goal;
import com.me.finalproject.pojo.Substitution;

public class EventDAO extends DAO{

	public Goal addGoal(Goal goal) {
		try {
		begin();
		getSession().save(goal);
		commit();
		close();
		return goal;
		} catch(HibernateException e) {
			
			System.out.println(e.getMessage());
		}
	return null;
	}
	
	public Substitution addSubstitution(Substitution sub) {
		try {
		begin();
		getSession().save(sub);
		commit();
		close();
		return sub;
		} catch(HibernateException e) {
			
			System.out.println(e.getMessage());
		}
	return null;
	}
	
	public Discipline addDiscipline(Discipline discipline) {
		try {
		begin();
		getSession().save(discipline);
		commit();
		close();
		return discipline;
		} catch(HibernateException e) {
			
			System.out.println(e.getMessage());
		}
	return null;
	}
	
}
