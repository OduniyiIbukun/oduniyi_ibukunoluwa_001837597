package com.me.finalproject.dao;

import java.util.ArrayList;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import com.me.finalproject.pojo.Message;
import com.me.finalproject.pojo.Team;
import com.me.finalproject.pojo.User;

public class MessageDAO extends DAO{

	public Message addMessage(Message message) throws Exception {
		try {
			begin();
			getSession().save(message);
			commit();
			close();
			return message;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Exception while creating team: " + e.getMessage());
		}
	}
	
	public Message getMessage(int number) {
		begin();
		Criteria criteria = getSession().createCriteria(Message.class)
				.add(Restrictions.eq("messageID", number));
		Message message = (Message) criteria.uniqueResult();
		close();
		return message;
	}
	
	public Message updateMessage(Message message) {
		begin();
		getSession().update(message);
		commit();
		close();
		return message;
		
	}
	
	public ArrayList<Message> getMessages(User u) {
		begin();
		Criteria criteria = getSession().createCriteria(Message.class).createAlias("receiver", "to")
				.add(Restrictions.eq("to.id", u.getId()));
		ArrayList<Message> messages = (ArrayList<Message>) criteria.list();
		close();
		return messages;
	}
	
}
