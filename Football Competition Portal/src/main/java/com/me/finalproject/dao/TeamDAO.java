package com.me.finalproject.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import com.me.finalproject.pojo.Team;

public class TeamDAO extends DAO {

	public Team addTeam(Team team) throws Exception {
		try {
			begin();
			getSession().save(team);
			commit();
			close();
			return team;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Exception while creating team: " + e.getMessage());
		}
	}

	public ArrayList<Team> getTeams() throws Exception {
		try {
			begin();
			Query q = getSession().createQuery("from Team");
			ArrayList<Team> teams = (ArrayList<Team>) q.list();
			close();
			return teams;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Could not get Teams " + e);
		}
	}
	
	public Team getTeam(String teamName) throws Exception{
		try {
			begin();
			System.out.println("inside DAO");
			Query q = getSession().createQuery("From Team where teamName = :teamName");
			q.setString("teamName", teamName);
			Team team = (Team) q.uniqueResult();
			close();
			return team;
		} catch(HibernateException e) {
			System.out.println("Error Encountered "+e.getMessage());
			rollback();
			throw new Exception("Could not get user " +e);
		}
		
	}
	
	public Team getTeamInitialized(String teamName) throws Exception{
		try {
			begin();
			System.out.println("inside DAO");
			Criteria criteria = getSession().createCriteria(Team.class).add(Restrictions.eq("teamName", teamName));
			Team team = (Team) criteria.uniqueResult();
			Hibernate.initialize(team.getPlayers());
			close();
			return team;
		} catch(HibernateException e) {
			System.out.println("Error Encountered "+e.getMessage());
			rollback();
			throw new Exception("Could not get user " +e);
		}
		
	}
	
	
	public ArrayList<Team> getTeams( String group) throws Exception{
		try {
			begin();
			Query q = getSession().createQuery("from Team where group = :group");
			q.setString("group", group);
			ArrayList<Team> teams = (ArrayList<Team>) q.list();
			close();
			return teams;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Could not get Teams " + e);
		}
	}

}
