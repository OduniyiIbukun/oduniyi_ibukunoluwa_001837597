package com.me.finalproject.dao;

import org.hibernate.HibernateException;

import com.me.finalproject.pojo.GroupFixture;

public class GroupFixtureDAO extends DAO{
 public GroupFixture addGroupFixture( GroupFixture gf) {
		try {
			begin();
			System.out.println("inside DAO");
			getSession().save(gf);
			commit();
			close();
			return gf;

		} catch (HibernateException e) {
			rollback();
			System.out.println(e.getMessage());
		}
		return null;
 }
}
