package com.me.finalproject.dao;

import org.hibernate.HibernateException;

import com.me.finalproject.pojo.KnockoutFixture;

public class KnockoutDAO extends DAO {
	 public KnockoutFixture addKOFixture( KnockoutFixture kof) {
			try {
				begin();
				System.out.println("inside DAO");
				getSession().save(kof);
				commit();
				close();
				return kof;

			} catch (HibernateException e) {
				rollback();
				System.out.println(e.getMessage());
			}
			return null;
	 }
}
