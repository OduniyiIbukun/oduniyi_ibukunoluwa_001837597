package com.me.finalproject.dao;

import java.util.ArrayList;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import com.me.finalproject.pojo.Coach;
import com.me.finalproject.pojo.Fixture;
import com.me.finalproject.pojo.Team;

public class CoachDAO extends DAO{

	public Coach register(Coach c) {
		try {
			begin();
			System.out.println("inside DAO");
			getSession().save(c);
			commit();
			close();
			return c;

		} catch (HibernateException e) {
			rollback();
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	public ArrayList<Coach> getCoaches(){
		begin();
		Criteria criteria = getSession().createCriteria(Coach.class);
		ArrayList<Coach> coaches = (ArrayList<Coach>) criteria.list();
		close();
		return coaches;
		
	}
	
	public Coach getCoach(Team team) {
		begin();
		Criteria criteria = getSession().createCriteria(Coach.class).createAlias("team", "team1")
				.add(Restrictions.eq("team1.teamName", team.getTeamName()));
		Coach coach = (Coach) criteria.uniqueResult();
		close();
		return coach;
	}
	
//	public Coach getCoachInitialized( long coachID) throws Exception{
//		try {
//		begin();
//		System.out.println("Inside CoachDAO");
//		Criteria criteria = getSession().createCriteria(F.class).
//				add(Restrictions.eq("matchID", fixtureID));
//
//		Fixture fixture  = (Fixture) criteria.uniqueResult();
//		Hibernate.initialize(fixture.getEvents());
//		close();
//		return fixture;
//	} catch (HibernateException e) {
//		System.out.println("Could not get Fixture "+e);
//		rollback();
//		throw new Exception("Could not get Fixture " + e);
//	}
//	}
	
}
