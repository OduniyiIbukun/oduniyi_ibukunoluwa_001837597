package com.me.finalproject.dao;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import com.me.finalproject.pojo.Coach;
import com.me.finalproject.pojo.Fixture;
import com.me.finalproject.pojo.User;


public class UserDAO extends DAO {
	
	
	

	public UserDAO() {
	}

	public User get(String userEmail, String password) throws Exception {
		try {
			begin();
			Query q = getSession().createQuery("from User where userEmail = :useremail and password = :password");
			q.setString("useremail", userEmail);
			q.setString("password", password);			
			User user = (User) q.uniqueResult();
			close();
			return user;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Could not get user " + userEmail, e);
		}
	}
	
	public User get(String userEmail){
		try {
			begin();
			Query q = getSession().createQuery("from User where userEmail = :useremail");
			q.setString("useremail", userEmail);
			User user = (User) q.uniqueResult();
			close();
			return user;
			
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
			return null;
		
	}
	
	public void deleteUser(User u) {
		begin();
		System.out.println("inside DAO");
		getSession().delete(u);
		commit();
		close();
	}
	

	public User register(User u) throws Exception {
		try {
			begin();
			System.out.println("inside DAO");
			getSession().save(u);
			commit();
			close();
			return u;

		} catch (HibernateException e) {
			rollback();
			throw new Exception("Exception while creating user: " + e.getMessage());
		}
	}
	
	public boolean updateUser(User u) {
		begin();
		getSession().update(u);
		commit();
		close();
		return true;
	}
	
	public boolean updateUser(String email) throws Exception {
		try {
			begin();
			System.out.println("inside DAO");
			Query q = getSession().createQuery("from User where userEmail = :useremail");
			q.setString("useremail", email);
			User user = (User) q.uniqueResult();
			if(user!=null){
				user.setStatus(1);
				getSession().update(user);
				commit();
				close();
				return true;
			}else{
				return false;
			}

		} catch (HibernateException e) {
			rollback();
			throw new Exception("Exception while creating user: " + e.getMessage());
		}
	
	}

	public User InitializeUser(User user) throws Exception{
		try {
		begin();
		System.out.println("Inside CoachDAO");
		Criteria criteria = getSession().createCriteria(User.class).add
		(Restrictions.and((Restrictions.eq
				("userEmail", user.getUserEmail())),(Restrictions.eq("password", user.getPassword()))));
		User u = (User) criteria.uniqueResult();
				Hibernate.initialize(u.getMessages());
		close();
		return u;
	} catch (HibernateException e) {
		System.out.println("Could not Initialize User "+e);
		rollback();
		throw new Exception("Could not Initialize User " + e);
	}
	}
	
	
	public User getUser(long id) {
		begin();
		Criteria crit = getSession().createCriteria(User.class);
		crit.add(Restrictions.eq("id", id)).uniqueResult();
		User user = (User) crit;
		close();
		return user;
		
	}


}