package com.me.finalproject.dao;

import java.util.ArrayList;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import com.me.finalproject.pojo.Referee;
import com.me.finalproject.pojo.Team;

public class RefereeDAO extends DAO {

	public Referee register(Referee r) {
		try {
			begin();
			System.out.println("inside DAO");
			getSession().save(r);
			commit();
			close();
			return r;

		} catch (HibernateException e) {
			rollback();
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	public ArrayList<Referee> getRefs() throws Exception {
		try {
			begin();
			Query q = getSession().createQuery("from Referee");
			ArrayList<Referee> refs = (ArrayList<Referee>) q.list();
			close();
			return refs;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Could not get Referees " +e.getMessage());
		}
	}
	
	public Referee getRef(Integer refID) throws Exception{
	try {
		begin();
		Query q = getSession().createQuery("from Referee where personnelID = :PersonnelID");
		q.setInteger("PersonnelID", refID );
		Referee referee = (Referee) q.uniqueResult();
		close();
		return referee;
	} catch(HibernateException e) {
		System.out.println("Error Message"+e.getMessage());
		throw new Exception("Could not get Referee" +e);
	}
	}
	
}
