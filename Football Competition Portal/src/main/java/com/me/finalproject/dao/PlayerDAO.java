package com.me.finalproject.dao;

import java.util.ArrayList;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.me.finalproject.pojo.Player;
import com.me.finalproject.pojo.User;

public class PlayerDAO extends DAO {

	public Player register(Player p) {
		try {
			begin();
			System.out.println("inside DAO");
			getSession().save(p);
			commit();
			close();
			return p;

		} catch (HibernateException e) {
			rollback();
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	public Player getPlayer(int id) {
		try {
			begin();
		Criteria criteria = getSession().createCriteria(Player.class);
		criteria.add(Restrictions.eq("personnelID", id));
		Player player = (Player) criteria.uniqueResult();
		Hibernate.initialize(player.getFixtures());
		Hibernate.initialize(player.getSelectedFixtures());
		close();
		return player;
		} catch(HibernateException e) {
			
			System.out.println(e.getMessage());
		}
	return null;
	}
	
	public ArrayList<Player> getPlayersView(){
		begin();
		Criteria criteria = getSession().createCriteria(Player.class).createAlias("team", "playerteam")
				.addOrder( Order.asc("playerteam.teamName"));
		ArrayList<Player> players = (ArrayList<Player>) criteria.list();
		close();
		return players;
	}
	
	
	
	public void initializeFixtures(Player player) {
		begin();
		System.out.println("inside initializer DAO");
		Hibernate.initialize(player.getFixtures());
		close();
	}
	
}
