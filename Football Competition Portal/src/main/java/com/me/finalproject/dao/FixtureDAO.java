package com.me.finalproject.dao;

import java.util.ArrayList;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.annotations.Immutable;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.me.finalproject.pojo.Fixture;
import com.me.finalproject.pojo.Player;
import com.me.finalproject.pojo.Team;
public class FixtureDAO extends DAO {

	public ArrayList<Fixture> getFixtures() throws Exception{
		try {
			begin();
			Query q = getSession().createQuery("from Fixture");
			ArrayList<Fixture> fixtures  = (ArrayList<Fixture>) q.list();
			close();
			return fixtures;
		} catch (HibernateException e) {
			rollback();
			throw new Exception("Could not get Fixtures " + e);
		
	}
	}
	
	public ArrayList<Fixture> getFixtures( Team team) throws Exception{
		try {
		System.out.println("Inside FixtureDAO");
		Criteria criteria = getSession().createCriteria(Fixture.class).createAlias("team1", "firstTeam").
				createAlias("team2", "secondTeam").add(Restrictions.disjunction().
						add(Restrictions.eq("firstTeam.teamName", team.getTeamName()))
				.add(Restrictions.eq("secondTeam.teamName", team.getTeamName())));
//		Query q = getSession().createQuery()
		ArrayList<Fixture> fixtures  = (ArrayList<Fixture>) criteria.list();
		close();
		return fixtures;
	} catch (HibernateException e) {
		System.out.println("Could not get Fixtures "+e);
		rollback();
		throw new Exception("Could not get Fixtures " + e);
	}
	}
	
	public ArrayList<Fixture> getFixtures( int refID) throws Exception{
		try {
		begin();
		System.out.println("Inside FixtureDAO");
		Criteria criteria = getSession().createCriteria(Fixture.class).createAlias("ref", "referee")
				.add(Restrictions.eq("referee.personnelID", refID));
//		Query q = getSession().createQuery()
		ArrayList<Fixture> fixtures  = (ArrayList<Fixture>) criteria.list();
		close();
		return fixtures;
	} catch (HibernateException e) {
		System.out.println("Could not get Fixtures "+e);
		rollback();
		throw new Exception("Could not get Fixtures " + e);
	}
	}
	
	public Fixture getFixture( int fixtureID ) throws Exception{
		try {
		begin();
		System.out.println("Inside FixtureDAO");
		Criteria criteria = getSession().createCriteria(Fixture.class).
				add(Restrictions.eq("matchID", fixtureID));

		Fixture fixture  = (Fixture) criteria.uniqueResult();
		close();
		return fixture;
	} catch (HibernateException e) {
		System.out.println("Could not get Fixture "+e);
		rollback();
		throw new Exception("Could not get Fixture " + e);
	}
	}
		
		@Immutable
		public ArrayList<Fixture> getFixturesView(){
			begin();
			Criteria criteria = getSession().createCriteria(Fixture.class).createAlias("team1", "firstteam")
			.createAlias("team2", "secondteam").createAlias("ref","official").createAlias("stadium", "venue")		
			.addOrder(Order.asc("matchType")).addOrder(Order.asc("fixtureDate"));
			ArrayList<Fixture> fixtures = (ArrayList<Fixture>) criteria.list();
			close();
			return fixtures;
		}
		
		public boolean updateFixture(Fixture f) throws Exception {
			try {
				begin();
				System.out.println("inside DAO");
				if(f!=null){
					getSession().update(f);
					commit();
					close();
					return true;
				}else{
					return false;
				}

			} catch (HibernateException e) {
				rollback();
				throw new Exception("Exception while updating fixture: " + e.getMessage());
			}	
		}
	
	
	public Fixture getFixtureInitialized( int fixtureID ) throws Exception{
		try {
			begin();
		System.out.println("Inside FixtureDAO");
		Criteria criteria = getSession().createCriteria(Fixture.class).
				add(Restrictions.eq("matchID", fixtureID));

		Fixture fixture  = (Fixture) criteria.uniqueResult();
		Hibernate.initialize(fixture.getEvents());
		close();
		return fixture;
	} catch (HibernateException e) {
		System.out.println("Could not get Fixture "+e);
		rollback();
		throw new Exception("Could not get Fixture " + e);
	}
	}
	
	}
