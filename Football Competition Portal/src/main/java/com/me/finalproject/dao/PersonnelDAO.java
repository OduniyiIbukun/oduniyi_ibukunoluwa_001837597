package com.me.finalproject.dao;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import com.me.finalproject.pojo.Coach;
import com.me.finalproject.pojo.Personnel;
import com.me.finalproject.pojo.Player;
import com.me.finalproject.pojo.User;

public class PersonnelDAO extends DAO {

	
	public Personnel register(Personnel p) throws Exception {
		try {
			begin();
			System.out.println("inside DAO");
			getSession().save(p);
			commit();
			close();
			return p;

		} catch (HibernateException e) {
			rollback();
			throw new Exception("Exception while creating Person: " + e.getMessage());
		}
	}
	
	public Player getPlayer(int id) {
		try {
			begin();
		Criteria criteria = getSession().createCriteria(Personnel.class);
		criteria.add(Restrictions.eq("personnelID", id));
		Player player = (Player) criteria.uniqueResult();
		Hibernate.initialize(player.getFixtures());
		Hibernate.initialize(player.getSelectedFixtures());
		close();
		return player;
		} catch(HibernateException e) {
			
			System.out.println(e.getMessage());
		}
	return null;
	}
	
	public boolean updatePlayer(Player p) throws Exception {
		try {
			begin();
			System.out.println("inside DAO");
			if(p!=null){
				getSession().update(p);
				commit();
				close();
				return true;
			}else{
				return false;
			}

		} catch (HibernateException e) {
			rollback();
			throw new Exception("Exception while updating user: " + e.getMessage());
		}	
	}
	
	public boolean updateCoach(Coach c) throws Exception {
		try {
			begin();
			System.out.println("inside DAO");
			if(c!=null){
				getSession().update(c);
				commit();
				close();
				return true;
			}else{
				return false;
			}

		} catch (HibernateException e) {
			rollback();
			throw new Exception("Exception while updating coach: " + e.getMessage());
		}	
	}
	
}
