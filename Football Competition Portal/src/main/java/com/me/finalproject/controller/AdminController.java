package com.me.finalproject.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.me.finalproject.dao.CoachDAO;
import com.me.finalproject.dao.EventDAO;
import com.me.finalproject.dao.FixtureDAO;
import com.me.finalproject.dao.GroupFixtureDAO;
import com.me.finalproject.dao.KnockoutDAO;
import com.me.finalproject.dao.MessageDAO;
import com.me.finalproject.dao.PersonnelDAO;
import com.me.finalproject.dao.PlayerDAO;
import com.me.finalproject.dao.RefereeDAO;
import com.me.finalproject.dao.StadiumDAO;
import com.me.finalproject.dao.TeamDAO;
import com.me.finalproject.dao.UserDAO;
import com.me.finalproject.pojo.Coach;
import com.me.finalproject.pojo.Discipline;
import com.me.finalproject.pojo.Event;
import com.me.finalproject.pojo.Fixture;
import com.me.finalproject.pojo.Goal;
import com.me.finalproject.pojo.GroupFixture;
import com.me.finalproject.pojo.KnockoutFixture;
import com.me.finalproject.pojo.Player;
import com.me.finalproject.pojo.Referee;
import com.me.finalproject.pojo.Stadium;
import com.me.finalproject.pojo.Substitution;
import com.me.finalproject.pojo.Team;

@Controller
public class AdminController {

	@Autowired
	@Qualifier("teamDAO")
	TeamDAO teamDAO;

	@Autowired
	UserDAO userDAO;

	@Autowired
	RefereeDAO refDAO;
	
	@Autowired
	MessageDAO messageDAO;
	
	@Autowired
	PersonnelDAO personDAO;
	
	@Autowired
	StadiumDAO stadiumDAO;
	
	@Autowired
	KnockoutDAO knockoutDAO;
	
	@Autowired
	GroupFixtureDAO groupFixtureDAO;
	
	@Autowired
	EventDAO eventDAO;
	
	@Autowired
	CoachDAO coachDAO;
	
	@Autowired
	FixtureDAO fixtureDAO;
	
	@Autowired
	PlayerDAO playerDAO;
	
	@RequestMapping(value = "/admin/createteam", method = RequestMethod.GET)
	public String addTeam() {
		return "create-team";
	}

	@RequestMapping(value = "/admin/createteam", method = RequestMethod.POST)
	public String createTeam(HttpServletRequest request, ModelMap map) {

		String teamName = request.getParameter("teamname");
		String group = request.getParameter("group");

		Team t = new Team();
		t.setTeamName(teamName);
		t.setGroup(group);

		try {
			Team team = teamDAO.addTeam(t);
			map.addAttribute("addteamsuccess", "Successfully added team");
			return "addteam-success";
		} catch (Exception e) {
			map.addAttribute("addteamfail", "Encountered an error when trying to add team");
			return "addteam-error";
		}

	}

	@RequestMapping(value = "/admin/createstadium", method = RequestMethod.GET)
	public String addStadium(StadiumDAO stadiumDAO, ModelMap map) {
		try {
			ArrayList<Stadium> stadiums = stadiumDAO.getStadiums();
			map.addAttribute("stadiums", stadiums);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error Message:" + e.getMessage());
		}
		return "create-stadium";
	}

	@RequestMapping(value = "/admin/createstadium.htm", method = RequestMethod.POST)
	public String createStadium(HttpServletRequest request, ModelMap map) {
		String stadiumName = request.getParameter("name");
		String stadiumLocation = request.getParameter("location");
		int capacity = Integer.valueOf(request.getParameter("capacity"));

		Stadium stad = new Stadium();
		stad.setStadiumName(stadiumName);
		stad.setStadiumLocation(stadiumLocation);
		stad.setCapacity(capacity);

		Stadium stadium = stadiumDAO.addStadium(stad);
		try {
			ArrayList<Stadium> stadiums = stadiumDAO.getStadiums();
			map.addAttribute("stadiums", stadiums);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error Message:" + e.getMessage());
		}
		return "create-stadium";

	}

	@RequestMapping(value = "/admin/selectgroup.htm", method = RequestMethod.POST)
	public String selectGroup(HttpServletRequest request, ModelMap map) {
		String group = request.getParameter("group");

		try {
			ArrayList<Team> teams = teamDAO.getTeams(group);
			ArrayList<Referee> refs = refDAO.getRefs();
			ArrayList<Stadium> stadiums = stadiumDAO.getStadiums();
			map.addAttribute("group", group);
			map.addAttribute("teams", teams);
			map.addAttribute("referees", refs);
			map.addAttribute("stadiums", stadiums);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error Message:" + e.getMessage());
		}
		return "create-groupfixture";
	}

	@RequestMapping(value = "/admin/createfixture", method = RequestMethod.GET)
	public String createGroupFixture() {
		return "create-groupfixture";
	}

	@RequestMapping(value = "/admin/createfixture", method = RequestMethod.POST)
	public String createFixture(HttpServletRequest request) throws ParseException {
		String category = request.getParameter("category");
		String d = request.getParameter("date");
		String team1 = request.getParameter("team1");
		String team2 = request.getParameter("team2");
		String stadium = request.getParameter("stadium");
		int refereeNum = Integer.valueOf(request.getParameter("referee"));

		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date date = format.parse(d);
		try {
			if (category.equals("Group")) {
				String group = request.getParameter("group");
				GroupFixture gf = new GroupFixture();
				Team fteam1 = teamDAO.getTeam(team1);
				Team fteam2 = teamDAO.getTeam(team2);
				Stadium stad = stadiumDAO.getStadium(stadium);
				Referee ref = refDAO.getRef(refereeNum);
				gf.setFixtureDate(date);
				gf.setMatchType(category);
				gf.setRef(ref);
				gf.setTeam1(fteam1);
				gf.setTeam2(fteam2);
				gf.setStadium(stad);
				gf.setGroup(group);
				gf.setStatus("Not Played");

				GroupFixture groupfixture = groupFixtureDAO.addGroupFixture(gf);
				return "create-groupfixture";
			}

			if (category.equals("Knockout")) {
				String round = request.getParameter("round");
				KnockoutFixture kof = new KnockoutFixture();
				Team fteam1 = teamDAO.getTeam(team1);
				Team fteam2 = teamDAO.getTeam(team2);
				Stadium stad = stadiumDAO.getStadium(stadium);
				Referee ref = refDAO.getRef(refereeNum);
				kof.setFixtureDate(date);
				kof.setMatchType(category);
				kof.setRef(ref);
				kof.setTeam1(fteam1);
				kof.setTeam2(fteam2);
				kof.setStadium(stad);
				kof.setKnockoutStage(round);
				kof.setStatus("Not Played");

				KnockoutFixture kofixture = knockoutDAO.addKOFixture(kof);
				return "create-ko-fixture";
			}
		} catch (Exception e) {
			System.out.println("Error encountered" + e.getMessage());
		}
		return null;
	}

	@RequestMapping(value = "/admin/createknockoutfixture", method = RequestMethod.GET)
	public String getKnockoutFixture(HttpServletRequest request, ModelMap map) {

		try {
			ArrayList<Team> teams = teamDAO.getTeams();
			ArrayList<Referee> refs = refDAO.getRefs();
			ArrayList<Stadium> stadiums = stadiumDAO.getStadiums();
			map.addAttribute("teams", teams);
			map.addAttribute("referees", refs);
			map.addAttribute("stadiums", stadiums);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error Message:" + e.getMessage());
		}
		return "create-ko-fixture";

	}

	public String editFixture(HttpServletRequest request, ModelMap map) {
		int fixtureID = Integer.valueOf(request.getParameter("fixture"));

		try {
			Fixture fixture = fixtureDAO.getFixture(fixtureID);
			map.addAttribute("fixture", fixture);
			return "edit-fixture";

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		return null;
	}

	@RequestMapping(value = "/admin/addevent", method = RequestMethod.GET)
	public String selectEvent(HttpServletRequest request, ModelMap map) {
		int fixtureID = Integer.valueOf(request.getParameter("fixture"));
		try {
			Fixture fixture = fixtureDAO.getFixture(fixtureID);
			String team1 = fixture.getTeam1().getTeamName();
			String team2 = fixture.getTeam2().getTeamName();
			map.addAttribute("team1", team1);
			map.addAttribute("team2", team2);
			map.addAttribute("fixture", fixtureID);
			return "event-selector";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	@RequestMapping(value = "/admin/selectevent", method = RequestMethod.POST)
	public String getAddEvent(HttpServletRequest request, ModelMap map) {
		int fixtureID = Integer.valueOf(request.getParameter("fixture"));
		String category = request.getParameter("category");
		String t = request.getParameter("team");

		try {
			Fixture fixture = fixtureDAO.getFixtureInitialized(fixtureID);
			Team team = teamDAO.getTeamInitialized(t);
			Set<Player> players = team.getPlayers();
			Set<Player> selectedPlayers = new HashSet<Player>();
			if (category.equals("goal")) {
				Set<Goal> goalEvents = new HashSet<Goal>();

				for (Player p : players) {
					for (Fixture fix : p.getSelectedFixtures()) {
						if (fix.getMatchID() == fixture.getMatchID()) {
							selectedPlayers.add(p);
						}
					}

				}

				for (Event event : fixture.getEvents()) {
					if (event.getCategory().equals("Goal")) {
						goalEvents.add((Goal) event);
					}
				}
				map.addAttribute("team", team);
				map.addAttribute("selectedplayers", selectedPlayers);
				map.addAttribute("goalEvents", goalEvents);
				map.addAttribute("fixture", fixtureID);

				return "add-goal";
			} else if (category.equals("substitution")) {
				Set<Substitution> subEvents = new HashSet<Substitution>();

				for (Player p : players) {
					for (Fixture fix : p.getSelectedFixtures()) {
						if (fix.getMatchID() == fixture.getMatchID()) {
							selectedPlayers.add(p);
						}
					}

				}

				for (Event event : fixture.getEvents()) {
					if (event.getCategory().equals("substitution")) {
						subEvents.add((Substitution) event);
					}
				}
				map.addAttribute("team", team);
				map.addAttribute("selectedplayers", selectedPlayers);
				map.addAttribute("subevents", subEvents);
				map.addAttribute("fixture", fixtureID);
				return "add-substitution";
			} else if (category.equals("discipline")) {
				Set<Discipline> disciplines = new HashSet<Discipline>();

				for (Player p : players) {
					for (Fixture fix : p.getSelectedFixtures()) {
						if (fix.getMatchID() == fixture.getMatchID()) {
							selectedPlayers.add(p);
						}
					}

				}

				for (Event event : fixture.getEvents()) {
					if (event.getCategory().equals("Discipline")) {
						disciplines.add((Discipline) event);
					}
				}

				map.addAttribute("team", team);
				map.addAttribute("selectedplayers", selectedPlayers);
				map.addAttribute("disciplines", disciplines);
				map.addAttribute("fixture", fixtureID);
				return "add-discipline";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	@RequestMapping(value = "/admin/addgoal", method = RequestMethod.POST)
	public String addGoal(HttpServletRequest request, ModelMap map) {
		int fixtureID = Integer.valueOf(request.getParameter("fixture"));
		int time = Integer.valueOf(request.getParameter("time"));
		int playerID = Integer.valueOf(request.getParameter("player"));
		String teamName = request.getParameter("team");

		try {
			Fixture fixture = fixtureDAO.getFixtureInitialized(fixtureID);
			Player player = playerDAO.getPlayer(playerID);
			Team team = teamDAO.getTeamInitialized(teamName);
			Set<Player> players = team.getPlayers();
			Set<Player> selectedPlayers = new HashSet<Player>();
			Set<Goal> goalEvents = new HashSet<Goal>();

			for (Player p : players) {
				for (Fixture fix : p.getSelectedFixtures()) {
					if (fix.getMatchID() == fixture.getMatchID()) {
						selectedPlayers.add(p);
					}
				}

			}

			for (Event event : fixture.getEvents()) {
				if (event.getCategory().equals("Goal")) {
					goalEvents.add((Goal) event);
				}
			}
			
			
			Goal goal = new Goal();
			goal.setCategory("Goal");
			goal.setTeam(team);
			goal.setPlayer(player);
			goal.setEventTime(time);
			goal.setFixture(fixture);
			Goal g = eventDAO.addGoal(goal);

			// code segment that populates the add-goal page
			map.addAttribute("team", team);
			map.addAttribute("selectedplayers", selectedPlayers);
			map.addAttribute("goalevents", goalEvents);
			map.addAttribute("fixture", fixtureID);
			return "add-goal";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "add-goal";
	}

	@RequestMapping(value = "/admin/addsubstitution", method = RequestMethod.POST)
	public String addSub(HttpServletRequest request, ModelMap map) {

		int fixtureID = Integer.valueOf(request.getParameter("fixture"));
		int time = Integer.valueOf(request.getParameter("time"));
		int player1ID = Integer.valueOf(request.getParameter("player1"));
		int player2ID = Integer.valueOf(request.getParameter("player2"));
		String teamName = request.getParameter("team");

		try {
			Fixture fixture = fixtureDAO.getFixtureInitialized(fixtureID);
			Player player1 = playerDAO.getPlayer(player1ID);
			Player player2 = playerDAO.getPlayer(player2ID);
			Team team = teamDAO.getTeamInitialized(teamName);
			Set<Player> players = team.getPlayers();
			Set<Player> selectedPlayers = new HashSet<Player>();

			Set<Substitution> subEvents = new HashSet<Substitution>();

			for (Player p : players) {
				for (Fixture fix : p.getSelectedFixtures()) {
					if (fix.getMatchID() == fixture.getMatchID()) {
						selectedPlayers.add(p);
					}
				}

			}

			for (Event event : fixture.getEvents()) {
				if (event.getCategory().equals("Substitution")) {
					subEvents.add((Substitution) event);
				}
			}
			
			map.addAttribute("fixture", fixtureID);
			
			Substitution sub = new Substitution();
			sub.setCategory("Substitution");
			sub.setTeam(team);
			sub.setPlayer1(player1);
			sub.setPlayer2(player2);
			sub.setEventTime(time);
			sub.setFixture(fixture);
			Substitution subsitut = eventDAO.addSubstitution(sub);

			// code segment that populates the add-goal page
			map.addAttribute("team", team);
			map.addAttribute("selectedplayers", selectedPlayers);
			map.addAttribute("subevents", subEvents);
			map.addAttribute("fixture", fixtureID);
			return "add-substitution";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "add-substitution";
	}

	@RequestMapping(value = "/admin/adddiscipline", method = RequestMethod.POST)
	public String addDiscipline(HttpServletRequest request, ModelMap map) {

		int fixtureID = Integer.valueOf(request.getParameter("fixture"));
		int time = Integer.valueOf(request.getParameter("time"));
		int playerID = Integer.valueOf(request.getParameter("player"));
		String teamName = request.getParameter("team");
		String card = request.getParameter("card");
		String description = request.getParameter("description");

		try {
			
			
			Fixture fixture = fixtureDAO.getFixtureInitialized(fixtureID);
			Player player = playerDAO.getPlayer(playerID);
			Team team = teamDAO.getTeamInitialized(teamName);
			Set<Player> players = team.getPlayers();
			Set<Player> selectedPlayers = new HashSet<Player>();
			
			Set<Discipline> disciplines = new HashSet<Discipline>();

			for (Player p : players) {
				for (Fixture fix : p.getSelectedFixtures()) {
					if (fix.getMatchID() == fixture.getMatchID()) {
						selectedPlayers.add(p);
					}
				}

			}

			for (Event event : fixture.getEvents()) {
				if (event.getCategory().equals("Discipline")) {
					disciplines.add((Discipline) event);
				}
			}

			
			Discipline disc = new Discipline();

			disc.setCategory("Discipline");
			disc.setTeam(team);
			disc.setPlayer(player);
			disc.setEventTime(time);
			disc.setFixture(fixture);
			disc.setDescription(description);
			disc.setCard(card);
			
			Discipline p = eventDAO.addDiscipline(disc);

			// code segment that populates the add-goal page
			map.addAttribute("team", team);
			map.addAttribute("selectedplayers", selectedPlayers);
			map.addAttribute("disciplines", disciplines);
			map.addAttribute("fixture", fixtureID);
			
			return "add-discipline";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "add-discipline";
	}
	
	@RequestMapping(value = "admin/info", method = RequestMethod.GET)
	public String tournamentInfo(HttpServletRequest request, ModelMap map) {
		ArrayList<Player> squad = playerDAO.getPlayersView();
		ArrayList<Coach> coaches = coachDAO.getCoaches();
		ArrayList<Fixture> fixtures = fixtureDAO.getFixturesView();
		map.addAttribute("fixtures", fixtures);
		map.addAttribute("squaddetails", squad);
		map.addAttribute("coaches", coaches);
		return "tournament-info";
	}
	
	@RequestMapping(value ="admin/update", method = RequestMethod.GET)
	public String updateFixture(HttpServletRequest request, ModelMap map) {
	
		int fixtureID = Integer.valueOf(request.getParameter("fixture"));
		try {
			Fixture fixture = fixtureDAO.getFixture(fixtureID);
			String matchType = fixture.getMatchType();
			
			if(matchType.equals("Group"))
			{
			 GroupFixture match = (GroupFixture) fixture;
			 String group = match.getGroup();
				ArrayList<Team> teams = teamDAO.getTeams(group);
				map.addAttribute("teams", teams);
				
			}
			ArrayList<Referee> refs = refDAO.getRefs();
			ArrayList<Stadium> stadiums = stadiumDAO.getStadiums();
			map.addAttribute("fixtureID", fixtureID);
			map.addAttribute("referees", refs);
			map.addAttribute("stadiums", stadiums);
			map.addAttribute("fixture", fixture);
			return "edit-fixture";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		}
		
		
		@RequestMapping(value ="admin/update", method = RequestMethod.POST)
		public String updateFix(HttpServletRequest request, ModelMap map) throws ParseException {
		
			int fixtureID = Integer.valueOf(request.getParameter("fixture"));
			String team1 = request.getParameter("team1");
			String team2 = request.getParameter("team2");
			String status = request.getParameter("status");
			String stadiumName = request.getParameter("stadium");
			int attendance = Integer.valueOf(request.getParameter("attendance"));
			int refereeID = Integer.valueOf(request.getParameter("referee"));
			String d = request.getParameter("date");

			DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
			Date date = format.parse(d);
			
			
			try {
				Fixture fixture = fixtureDAO.getFixture(fixtureID);
				Team fteam1 = teamDAO.getTeam(team1);
				Team fteam2 = teamDAO.getTeam(team2);
				Stadium stad = stadiumDAO.getStadium(stadiumName);
				Referee ref = refDAO.getRef(refereeID);
				
				fixture.setRef(ref);
				fixture.setFixtureDate(date);
				fixture.setTeam1(fteam1);
				fixture.setTeam2(fteam2);
				fixture.setStadium(stad);
				fixture.setStatus(status);
				fixture.setAttendance(attendance);
			
				boolean updateStatus = fixtureDAO.updateFixture(fixture);
				
				if(updateStatus) {
					return "fixture-update-success";
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
			return null;
		
	}
		@RequestMapping(value = "/admin/searchteam", method = RequestMethod.POST)
		public String searchTeams(HttpServletRequest request, ModelMap map) {
			String teamName = request.getParameter("team");
			
			try {
				Team team = teamDAO.getTeamInitialized(teamName);
				Coach coach = team.getCoach();
				Set<Player> players = team.getPlayers();
				ArrayList<Fixture> fixtures = fixtureDAO.getFixtures(team);
				map.addAttribute("squaddetails", players);
				map.addAttribute("fixtures", fixtures);
				map.addAttribute("coach", coach);
				map.addAttribute("team",teamName);
				return "team-info";
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			map.addAttribute("errorMessage", "We don't have records for that team");
			return "error";
		}

}
