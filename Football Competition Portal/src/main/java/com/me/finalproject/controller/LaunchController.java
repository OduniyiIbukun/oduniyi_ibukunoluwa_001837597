package com.me.finalproject.controller;

import java.util.ArrayList;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.me.finalproject.dao.FixtureDAO;
import com.me.finalproject.dao.TeamDAO;
import com.me.finalproject.pojo.Fixture;
import com.me.finalproject.pojo.Team;

@Controller
public class LaunchController {

	@RequestMapping(value = "admin/start", method = RequestMethod.GET)
	public String Navigate(HttpServletRequest request, TeamDAO teamDAO, FixtureDAO fixtureDAO, ModelMap map) {

			try {
				ArrayList<Team> teams = teamDAO.getTeams();
				ArrayList<Fixture> fixtures = fixtureDAO.getFixtures();
				map.addAttribute("teams",teams);
				map.addAttribute("fixtures", fixtures);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "Admin-dashboard";
	}
	
	@RequestMapping(value = "user/start", method = RequestMethod.GET)
	public String Navigate(HttpServletRequest request) {
		return "user-login";
	}
	
	@RequestMapping(value = "admin/home", method = RequestMethod.GET)
	public String adminHome(HttpServletRequest request, FixtureDAO fixtureDAO, TeamDAO teamDAO, ModelMap map) {
		try {
			ArrayList<Team> teams = teamDAO.getTeams();
			ArrayList<Fixture> fixtures = fixtureDAO.getFixtures();
			map.addAttribute("teams",teams);
			map.addAttribute("fixtures", fixtures);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "Admin-dashboard";
	}

	@RequestMapping(value = "/selectcategory", method = RequestMethod.POST)
	public String userInput(HttpServletRequest request, ModelAndView mav, TeamDAO teamDAO, ModelMap map) {

		String category = request.getParameter("category");

		if (category.equals("Player")) {
			try {
				ArrayList<Team> teams = teamDAO.getTeams();
				map.addAttribute("teams", teams);
				map.addAttribute("category", category);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println("Couldn't retrieve all the records" + e.getMessage());
			}
			return "user-create-form";

		}

		else if (category.equals("Coach")) {
			try {
				ArrayList<Team> teams = teamDAO.getTeams();
				map.addAttribute("teams", teams);
				map.addAttribute("category", category);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println("Couldn't retrieve all the records" + e.getMessage());
			}
			return "create-coach";

		}

		else if (category.equals("Referee")) {

			map.addAttribute("category", category);
			return "referee-create";

		}

		return null;
	}
}
