package com.me.finalproject.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.swing.text.html.FormSubmitEvent.MethodType;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.me.finalproject.dao.CoachDAO;
import com.me.finalproject.dao.EventDAO;
import com.me.finalproject.dao.FixtureDAO;
import com.me.finalproject.dao.KnockoutDAO;
import com.me.finalproject.dao.MessageDAO;
import com.me.finalproject.dao.PersonnelDAO;
import com.me.finalproject.dao.PlayerDAO;
import com.me.finalproject.dao.RefereeDAO;
import com.me.finalproject.dao.StadiumDAO;
import com.me.finalproject.dao.TeamDAO;
import com.me.finalproject.dao.UserDAO;
import com.me.finalproject.pojo.Coach;
import com.me.finalproject.pojo.Fixture;
import com.me.finalproject.pojo.Message;
import com.me.finalproject.pojo.Player;
import com.me.finalproject.pojo.Team;
import com.me.finalproject.pojo.User;

@Controller
public class PlayerController {

	@Autowired
	@Qualifier("teamDAO")
	TeamDAO teamDAO;

	@Autowired
	UserDAO userDAO;

	@Autowired
	RefereeDAO refDAO;
	
	@Autowired
	MessageDAO messageDAO;
	
	@Autowired
	PersonnelDAO personDAO;
	
	@Autowired
	StadiumDAO stadiumDAO;
	
	@Autowired
	KnockoutDAO knockoutDAO;
	
	
	@Autowired
	EventDAO eventDAO;
	
	@Autowired
	CoachDAO coachDAO;
	
	@Autowired
	FixtureDAO fixtureDAO;
	
	@Autowired
	PlayerDAO playerDAO;
	
	@RequestMapping(value = "/player/register", method = RequestMethod.GET)
	public String fixtureRegistration(HttpServletRequest request,
			 ModelMap map) throws Exception 
	{
		
		int matchId =  Integer.valueOf(request.getParameter("fixture"));
		int playerId = Integer.valueOf(request.getParameter("player"));
		long id = Long.valueOf(request.getParameter("player"));
		try {
		Player player = personDAO.getPlayer(playerId);
		Fixture match = fixtureDAO.getFixture(matchId);
		
		player.getFixtures().add(match);
		
		boolean status = personDAO.updatePlayer(player);
		
		
		if(status) 
		{
			Team team = ((Player) player).getTeam();
			ArrayList<Fixture> fixtures = fixtureDAO.getFixtures(team);
			
;	
			User playerUser = player.getUser();
		Message message = new Message();
		message.setSender(playerUser);
		Date now = Calendar.getInstance().getTime(); 
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		String date = format.format(now);
		
		Date rightNow = format.parse(date);
		
		message.setMessageDate(rightNow);
		Coach c = coachDAO.getCoach(team);
		
		ArrayList<Coach> coaches = coachDAO.getCoaches();
		
		for(Coach coach: coaches) {
			if(coach.getTeam().getTeamName().equals(team.getTeamName())) {
				User coachUser = coach.getUser();
				message.setReceiver(coachUser);
				String fixtureDate = format.format(match.getFixtureDate());
				message.setContent("Hello, "+player.getFirstName()+" "+player.getLastName()+" has chosen to notify you of his "
						+ "availability for the match on "+fixtureDate);
				message.setStatus("Unread");
				Message msg = messageDAO.addMessage(message);
				User manager = userDAO.InitializeUser(coachUser);

				User plyr  = userDAO.InitializeUser(playerUser);
				
//				boolean playerStatus = userDAO.updateUser(plyr);
				boolean coachStatus = userDAO.updateUser(manager);

				if(coachStatus){
					map.addAttribute("fixtures", fixtures);
					map.addAttribute("player", player);
				}
			}	
		}	
	
		return "user-dashboard";
		}
		
		}
		catch(HibernateException e) {
			System.out.println("Could not add fixture to players fixtures");
		}
		return null;
	}

	@RequestMapping(value = "/player/unregister", method = RequestMethod.GET)
	public String fixtureUnregistration(HttpServletRequest request, ModelMap map) throws Exception {

		int matchId = Integer.valueOf(request.getParameter("fixture"));
		int playerId = Integer.valueOf(request.getParameter("player"));

		try {
			Player player = personDAO.getPlayer(playerId);
			Fixture match = fixtureDAO.getFixture(matchId);

			player.getFixtures().remove((match));
			boolean status = personDAO.updatePlayer(player);
			if (status) {
				// Hibernate.initialize(player.getFixtures());
				Team team = ((Player) player).getTeam();
				ArrayList<Fixture> fixtures = fixtureDAO.getFixtures(team);
				map.addAttribute("fixtures", fixtures);
				map.addAttribute("player", player);
				return "user-dashboard";
			}

		} catch (HibernateException e) {
			System.out.println("Could not remove fixture from player fixtures");
		}
		return null;
	}

}
