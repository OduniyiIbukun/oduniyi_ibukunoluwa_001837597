package com.me.finalproject.controller;

import static org.hamcrest.CoreMatchers.instanceOf;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.captcha.botdetect.web.servlet.Captcha;
import com.me.finalproject.dao.CoachDAO;
import com.me.finalproject.dao.EventDAO;
import com.me.finalproject.dao.FixtureDAO;
import com.me.finalproject.dao.KnockoutDAO;
import com.me.finalproject.dao.MessageDAO;
import com.me.finalproject.dao.PersonnelDAO;
import com.me.finalproject.dao.PlayerDAO;
import com.me.finalproject.dao.RefereeDAO;
import com.me.finalproject.dao.StadiumDAO;
import com.me.finalproject.dao.TeamDAO;
import com.me.finalproject.dao.UserDAO;
import com.me.finalproject.pojo.Coach;
import com.me.finalproject.pojo.Fixture;
import com.me.finalproject.pojo.GroupFixture;
import com.me.finalproject.pojo.Message;
import com.me.finalproject.pojo.Personnel;
import com.me.finalproject.pojo.Player;
import com.me.finalproject.pojo.Referee;
import com.me.finalproject.pojo.Team;
import com.me.finalproject.pojo.User;

//
/**
 * Handles requests for the application home page.
 */

@Controller
public class UserController {

	@Autowired
	@Qualifier("teamDAO")
	TeamDAO teamDAO;

	@Autowired
	UserDAO userDAO;

	@Autowired
	RefereeDAO refDAO;
	
	@Autowired
	MessageDAO messageDAO;
	
	@Autowired
	PersonnelDAO personDAO;
	
	@Autowired
	StadiumDAO stadiumDAO;
	
	@Autowired
	KnockoutDAO knockoutDAO;
	
	
	@Autowired
	EventDAO eventDAO;
	
	@Autowired
	CoachDAO coachDAO;
	
	@Autowired
	FixtureDAO fixtureDAO;
	
	@Autowired
	PlayerDAO playerDAO;
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@RequestMapping(value = "/user/login.htm", method = RequestMethod.GET)
	public String showLoginForm() {

		return "user-login";
	}

	@RequestMapping(value = "/user/home", method = RequestMethod.GET)
	public String userHome(HttpServletRequest request, ModelMap map) throws Exception {
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		try {
			Player player = (Player) user.getPerson();
			if (player instanceof Player) {
				Team team = ((Player) player).getTeam();
				ArrayList<Fixture> fixtures = fixtureDAO.getFixtures(team);
				Player p = personDAO.getPlayer(player.getPersonnelID());
				map.addAttribute("player", p);
				map.addAttribute("fixtures", fixtures);
				map.addAttribute("user", user);

				return "user-dashboard";
			}
		} catch (HibernateException e) {
			System.out.println("Error" + e.getMessage());
		}
		return null;
	}

	@RequestMapping(value = "/user/login.htm", method = RequestMethod.POST)
	public String handleLoginForm(HttpServletRequest request, ModelMap map) {

		String username = request.getParameter("username");
		String password = request.getParameter("password");

		try {
			User u = userDAO.get(username, password);

			if (u != null && u.getStatus() == 1) {
				HttpSession session = request.getSession();
				session.setAttribute("user", u);
				if (u.getPerson().getCategory().equals("Player")) {

					// Personnel player = u.getPerson();
					Player player = (Player) u.getPerson();
					if (player instanceof Player) {
						Team team = ((Player) player).getTeam();
						ArrayList<Fixture> fixtures = fixtureDAO.getFixtures(team);
						// ArrayList<Fixture> upcomingFixture = new ArrayList<Fixture>();
						// ArrayList<Fixture> playedFixture = new ArrayList<Fixture>();
						//
						// for (Fixture fixture: fixtures) {
						// if(fixture.getStatus().equals("Not Played")) {
						// upcomingFixture.add(fixture);
						// }
						// else if(fixture.getStatus().equals("Played")) {
						// playedFixture.add(fixture);
						// }
						// }

						// playerDAO.initializeFixtures(player);
						Player p = personDAO.getPlayer(player.getPersonnelID());
						map.addAttribute("player", p);
						// map.addAttribute("upcomingfixtures",upcomingFixture );
						// map.addAttribute("playedFixtures",playedFixture);
						map.addAttribute("fixtures", fixtures);
						map.addAttribute("user", u);
						return "user-dashboard";
					}
					map.addAttribute("errorMessage", "the person is not a player");
					return "error";

				} else if (u.getPerson().getCategory().equals("Coach")) {
					Coach coach = (Coach) u.getPerson();
					if (coach instanceof Coach) {
						User intializedUser = userDAO.InitializeUser(u);
						Team team = coach.getTeam();
						ArrayList<Fixture> fixtures = fixtureDAO.getFixtures(team);
						ArrayList<Message> messages = messageDAO.getMessages(intializedUser);

						map.addAttribute("fixtures", fixtures);
						map.addAttribute("team", team);
						map.addAttribute("user", intializedUser);
						map.addAttribute("messages", messages);
						return "coach-dashboard";
					}
					map.addAttribute("errorMessage", "the person is not a Coach");
					return "error";
				} else if (u.getPerson().getCategory().equals("Referee")) {
					Referee referee = (Referee) u.getPerson();
					ArrayList<Fixture> fixtures = fixtureDAO.getFixtures();
					map.addAttribute("fixtures", fixtures);
					map.addAttribute("user", u);
					return "referee-dashboard";
				}
			} else if (u != null && u.getStatus() == 0) {
				map.addAttribute("errorMessage", "Please activate your account to login!");
				return "error";
			} else {
				map.addAttribute("errorMessage", "Invalid username/password!");
				return "error";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error " + e.getMessage());
		}

		return null;
	}

	@RequestMapping(value = "/user/create.htm", method = RequestMethod.GET)
	public String showCreateForm() {

		return "account-select";
	}

	@RequestMapping(value = "/user/create.htm", method = RequestMethod.POST)
	public String handleCreateForm(HttpServletRequest request, ModelMap map) throws ParseException {

		String captchaCode = request.getParameter("captchaCode");

		Captcha captcha = Captcha.load(request, "CaptchaObject");

		HttpSession session = request.getSession();
		if (captcha.validate(captchaCode)) {
			String email = request.getParameter("username");
			String password = request.getParameter("password");
			String firstName = request.getParameter("firstname");
			String lastName = request.getParameter("lastname");
			String dob = request.getParameter("DOB");
			String category = request.getParameter("category");

			DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

			Date dateOfBirth = format.parse(dob);

			User u = new User();

			u.setUserEmail(email);
			u.setPassword(password);
			u.setStatus(0);

			try {

				User user = userDAO.register(u);
				Random random = new Random();

				int key1 = random.nextInt(50000);
				int key2 = random.nextInt(50000);

				String emailMessage = "http://localhost:8084/finalproject/user/validateemail.htm?email="
						+ user.getUserEmail() + "&key1=" + key1 + "&key2=" + key2;

				sendEmail(user.getUserEmail(), emailMessage);

				session.setAttribute("key1", key1);
				session.setAttribute("key2", key2);

				if (category.equals("Player")) {
					String team = request.getParameter("team");
					Team t = teamDAO.getTeam(team);
					
					
					int squadNumber = Integer.valueOf(request.getParameter("squadnumber"));
					String position = request.getParameter("position");

					Player player = new Player();
					player.setFirstName(firstName);
					player.setLastName(lastName);
					player.setCategory(category);
					player.setBirthdate(dateOfBirth);
					player.setSquadNumber(squadNumber);
					player.setPosition(position);
					user.setPerson(player);
					player.setUser(user);
					player.setTeam(t);

					Player players = playerDAO.register(player);
					map.addAttribute("person", players);
					return "player-create";
				}

				else if (category.equals("Coach")) {
					
				

					String team = request.getParameter("team");
					Team t = teamDAO.getTeam(team);

					if(t.getCoach() == null) {
					String qualification = request.getParameter("qual");
					String style = request.getParameter("style");

					Coach coach = new Coach();
					coach.setFirstName(firstName);
					coach.setLastName(lastName);
					coach.setCategory(category);
					coach.setBirthdate(dateOfBirth);
					coach.setCoachQualification(qualification);
					coach.setCoachStyle(style);
					user.setPerson(coach);
					coach.setUser(user);
					coach.setTeam(t);

					Coach coachie = coachDAO.register(coach);
					map.addAttribute("person", coach);
					return "player-create";
				}else{	
					userDAO.deleteUser(user);
					map.addAttribute("errorMessage","this team already has a coach");
					return "error";
					}
				}

				else if (category.equals("Referee")) {

					String refClass = request.getParameter("class");
					String nationality = request.getParameter("nationality");

					Referee referee = new Referee();
					referee.setFirstName(firstName);
					referee.setLastName(lastName);
					referee.setCategory(category);
					referee.setBirthdate(dateOfBirth);
					referee.setRefClass(refClass);
					referee.setNationality(nationality);
					user.setPerson(referee);
					referee.setUser(user);

					Referee ref = refDAO.register(referee);
					map.addAttribute("person", ref);
					return "player-create";
				}

			} catch (Exception e) {
				System.out.println("Exception " + e.getMessage());
				System.out.println("We couldn't register you");

			}
		} else {

			map.addAttribute("errorMessage", "Invalid Captcha. Re-enter again!");
			return "user-create-form";
		}
		return null;
	}

	@RequestMapping(value = "/user/forgotpassword.htm", method = RequestMethod.GET)
	public String getForgotPasswordForm(HttpServletRequest request) {

		return "forgot-password";
	}

	@RequestMapping(value = "/user/forgotpassword.htm", method = RequestMethod.POST)
	public String handleForgotPasswordForm(HttpServletRequest request) {
		String useremail = request.getParameter("useremail");
		Captcha captcha = Captcha.load(request, "CaptchaObject");
		String captchaCode = request.getParameter("captchaCode");

		if (captcha.validate(captchaCode)) {
			User user = userDAO.get(useremail);
			sendEmail(useremail, "Your password is : " + user.getPassword());
			return "forgot-password-success";
		}

		else {
			request.setAttribute("captchamsg", "Captcha not valid");
			return "forgot-password";
		}

	}

	@RequestMapping(value = "user/resendemail.htm", method = RequestMethod.POST)
	public String resendEmail(HttpServletRequest request) {
		HttpSession session = request.getSession();

		String email = request.getParameter("username");
		try {

			Random random = new Random();

			int key1 = random.nextInt(50000);
			int key2 = random.nextInt(50000);

			String emailMessage = "http://localhost:8084/finalproject/user/validateemail.htm?email=" + email + "&key1="
					+ key1 + "&key2=" + key2;

			sendEmail(email, emailMessage);

			session.setAttribute("key1", key1);
			session.setAttribute("key2", key1);
			return "user-created";

		} catch (Exception e) {
			System.out.println("Error Sending mail");
		}
		return "user-created";
	}

	public void sendEmail(String useremail, String message) {
		try {
			Email email = new SimpleEmail();

			email.setHostName("smtp.googlemail.com");
			email.setSmtpPort(465);
			email.setSSLOnConnect(true);

			email.setAuthenticator(new DefaultAuthenticator("contactapplication2018@gmail.com", "springmvc"));
			email.setFrom("no-reply@gmail.com");
			email.setSubject("Account Activation");
			email.setMsg(message);
			email.addTo(useremail);

			email.send();
		} catch (Exception e) {

			System.out.println("Email cannot be send");
		}

	}

	@RequestMapping(value = "user/validateemail.htm", method = RequestMethod.GET)
	public String validateEmail(HttpServletRequest request, ModelMap map) {

		// The user will be sent the following link when the use registers
		// This is the format of the email
		// http://hostname:8080/lab10/user/validateemail.htm?email=useremail&key1=<random_number>&key2=<body
		// of the email that when user registers>

		String email = request.getParameter("email");

		HttpSession session = request.getSession();

		int key1 = (Integer) session.getAttribute("key1");
		int key2 = (Integer) session.getAttribute("key2");

		int kr1 = Integer.parseInt(request.getParameter("key1"));
		int kr2 = Integer.parseInt(request.getParameter("key2"));

		if (key1 == kr1 && key2 == kr2) {
			try {
				boolean status = userDAO.updateUser(email);

				if (status) {
					return "user-login";

				} else {

					return "error";
				}
			} catch (Exception e) {
				System.out.println("Error updating user");
			}
		} else {

			map.addAttribute("errorMessage", "link expired, generate new link");
			map.addAttribute("resendLink", true);
			return "error";
		}
		return null;
	}

	@RequestMapping(value = "/user/searchteam", method = RequestMethod.POST)
	public String searchTeams(HttpServletRequest request, ModelMap map) {
		String teamName = request.getParameter("team");
		
		try {
			Team team = teamDAO.getTeamInitialized(teamName);
			Coach coach = team.getCoach();
			Set<Player> players = team.getPlayers();
			ArrayList<Fixture> fixtures = fixtureDAO.getFixtures(team);
			map.addAttribute("squaddetails", players);
			map.addAttribute("fixtures", fixtures);
			map.addAttribute("coach", coach);
			map.addAttribute("team",teamName);
			return "teaminfo";
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		map.addAttribute("errorMessage", "We don't have records for that team");
		return "error";
	}
	
	@RequestMapping(value = "/user/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request) {

		HttpSession session = request.getSession();
		session.invalidate();
		return "user-login";
	}

}
