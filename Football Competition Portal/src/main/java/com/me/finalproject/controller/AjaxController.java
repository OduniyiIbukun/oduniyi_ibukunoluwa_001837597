package com.me.finalproject.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonObject;
import com.me.finalproject.dao.FixtureDAO;
import com.me.finalproject.dao.TeamDAO;
import com.me.finalproject.pojo.Fixture;
import com.me.finalproject.pojo.Team;

@Controller
public class AjaxController {
	@Autowired
	TeamDAO teamDAO;
	
	@Autowired
	FixtureDAO fixtureDAO;
	
@RequestMapping(value = "/ajaxservice.htm", method  = RequestMethod.POST)
@ResponseBody
public String getTeams(HttpServletRequest request){
	
	try {
		ArrayList<Team> teams = teamDAO.getTeams();
	System.out.println("I got to this point");
	String queryString = request.getParameter("teamname");
	String result = "";
	
	for(Team team: teams) {
		if(team.getTeamName().toLowerCase().contains(queryString.toLowerCase())) {
			result += team.getTeamName()+",";
		}
	}
	
	
//	for(int i =0;i<teams.size();i++){
//		if(teams.get(i).toLowerCase().contains(queryString.toLowerCase())){
//			result +=courseList.get(i)+",";
//		}
//	}
	
	return result;
	} catch (Exception e) {
		// TODO Auto-generated catch block
		System.out.println("Error retreiving the team info "+e.getMessage());
	}
	return "happy";
}

@RequestMapping(value = "/admin/ajaxteams", method  = RequestMethod.GET)
public String getTeam(HttpServletRequest request){
	
	try {
	ArrayList<Team> teams = teamDAO.getTeams();
	System.out.println("I got to this point");
	String queryString = request.getParameter("teamname");
	
	Team team = teamDAO.getTeam(queryString);
	
	ArrayList<Fixture> fixtures = fixtureDAO.getFixtures(team);
	

	JsonObject fixtureJSON = new JsonObject();
	DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
	for(Fixture fixture : fixtures) {
		String date = format.format(fixture.getFixtureDate());
		fixtureJSON.addProperty("fixturedate", date);
		fixtureJSON.addProperty("team1", fixture.getTeam1().getTeamName());
		fixtureJSON.addProperty("team2", fixture.getTeam2().getTeamName());
		fixtureJSON.addProperty("matchtype", fixture.getMatchType());
		fixtureJSON.addProperty("stadium", fixture.getStadium().getStadiumName());
		fixtureJSON.addProperty("refereefirstname", fixture.getRef().getFirstName());
		fixtureJSON.addProperty("refereelastname", fixture.getRef().getLastName());
	}
	
	return fixtureJSON.toString();
	
	} catch (Exception e) {
		// TODO Auto-generated catch block
		System.out.println("Error retreiving the team info "+e.getMessage());
	}
	return "happy";
}

}
