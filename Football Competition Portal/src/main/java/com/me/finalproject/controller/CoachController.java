package com.me.finalproject.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.me.finalproject.dao.CoachDAO;
import com.me.finalproject.dao.EventDAO;
import com.me.finalproject.dao.FixtureDAO;
import com.me.finalproject.dao.MessageDAO;
import com.me.finalproject.dao.PersonnelDAO;
import com.me.finalproject.dao.PlayerDAO;
import com.me.finalproject.dao.TeamDAO;
import com.me.finalproject.dao.UserDAO;
import com.me.finalproject.pojo.Coach;
import com.me.finalproject.pojo.Fixture;
import com.me.finalproject.pojo.Message;
import com.me.finalproject.pojo.Player;
import com.me.finalproject.pojo.Team;
import com.me.finalproject.pojo.User;



@Controller
public class CoachController {

	@Autowired
	@Qualifier("teamDAO")
	TeamDAO teamDAO;

	@Autowired
	UserDAO userDAO;
	
	@Autowired
	MessageDAO messageDAO;
	
	@Autowired
	PersonnelDAO personDAO;
	
	@Autowired
	EventDAO eventDAO;
	
	@Autowired
	CoachDAO coachDAO;
	
	@Autowired
	FixtureDAO fixtureDAO;
	
	@Autowired
	PlayerDAO playerDAO;
	
	@RequestMapping(value = "/coach/viewregisteredplayers", method = RequestMethod.GET)
	public String viewRegisteredPlayer(HttpServletRequest request,FixtureDAO fixtureDAO, TeamDAO teamDAO, ModelMap map) {

		int fixtureID = Integer.valueOf(request.getParameter("fixture"));
		String teamName = request.getParameter("team");

		try {
			Fixture fixture = fixtureDAO.getFixture(fixtureID);
			Team team = teamDAO.getTeamInitialized(teamName);
			Set<Player> players = team.getPlayers();
			Set<Player> registeredPlayers = new HashSet<Player>();
			for (Player player : players) {
				for (Fixture fix : player.getFixtures()) {
					if (fix.getMatchID() == fixture.getMatchID()) {
						registeredPlayers.add(player);
					}
				}
			}

			Set<Player> selectedPlayers = new HashSet<Player>();
			// Checking players who are selected for this fixture
			for (Player p : players) {
				for (Fixture fix : p.getSelectedFixtures()) {
					if (fix.getMatchID() == fixture.getMatchID()) {
						selectedPlayers.add(p);
					}
				}
			}

			map.addAttribute("selectedplayers", selectedPlayers);
			map.addAttribute("team", teamName);
			map.addAttribute("fixture", fixture);
			map.addAttribute("registeredplayers", registeredPlayers);
			return "select-squad";

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public boolean checkSelected(Player p, Fixture f) {

		for (Fixture fixture : p.getSelectedFixtures()) {
			if (fixture.getMatchID() == f.getMatchID()) {
				return true;
			}
		}

		return false;
	}

	@RequestMapping(value = "/coach/select", method = RequestMethod.GET)
	public String selectSquad(HttpServletRequest request, FixtureDAO fixtureDAO, PlayerDAO playerDAO, 
			PersonnelDAO personDAO, TeamDAO teamDAO,  ModelMap map) {
		int fixtureID = Integer.valueOf(request.getParameter("fixture"));
		int playerID = Integer.valueOf(request.getParameter("player"));
		String teamName = request.getParameter("team");

		try {
			Fixture fixture = fixtureDAO.getFixture(fixtureID);
			Player player = playerDAO.getPlayer(playerID);
			
			boolean status = checkSelected(player, fixture);

			if (status == false) {
				player.getSelectedFixtures().add(fixture);
				personDAO.updatePlayer(player);

				Set<Player> selectedPlayers = new HashSet<Player>();
				Team team = teamDAO.getTeamInitialized(teamName);

				Set<Player> players = team.getPlayers();

				// Checking players who are selected for this fixture
				for (Player p : players) {
					for (Fixture fix : p.getSelectedFixtures()) {
						if (fix.getMatchID() == fixture.getMatchID()) {
							selectedPlayers.add(p);
						}
					}
				}

				Set<Player> registeredPlayers = new HashSet<Player>();
				for (Player plyer : players) {
					for (Fixture fix : plyer.getFixtures()) {
						if (fix.getMatchID() == fixture.getMatchID()) {
							registeredPlayers.add(plyer);
						}
					}
				}

				map.addAttribute("selectedplayers", selectedPlayers);
				map.addAttribute("team", teamName);
				map.addAttribute("fixture", fixture);
				map.addAttribute("registeredplayers", registeredPlayers);
				return "select-squad";
			} else {
				map.addAttribute("errorMessage", "The player has already been selected for this fixture");
				return "error";
			}
		}

		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		map.addAttribute("errorMessage", "The player has already been selected for this fixture");
		return "error";
	}

	@RequestMapping(value = "/coach/unselect", method = RequestMethod.GET)
	public String UnselectPlayer(HttpServletRequest request, FixtureDAO fixtureDAO, PersonnelDAO personDAO,
			TeamDAO teamDAO, ModelMap map) {
		int fixtureID = Integer.valueOf(request.getParameter("fixture"));
		int playerID = Integer.valueOf(request.getParameter("player"));
		String teamName = request.getParameter("team");

		try {
			Fixture fixture = fixtureDAO.getFixture(fixtureID);
			Player player = personDAO.getPlayer(playerID);
			player.getSelectedFixtures().remove(fixture);
			boolean status = personDAO.updatePlayer(player);
			if (status) {
				Set<Player> selectedPlayers = new HashSet<Player>();
				Team team = teamDAO.getTeamInitialized(teamName);
				Set<Player> players = team.getPlayers();

				// Checking players who are selected for this fixture
				for (Player p : players) {
					for (Fixture fix : p.getSelectedFixtures()) {
						if (fix.getMatchID() == fixture.getMatchID()) {
							selectedPlayers.add(p);
						}
					}
				}

				Set<Player> registeredPlayers = new HashSet<Player>();

				for (Player plyer : players) {
					for (Fixture fix : plyer.getFixtures()) {
						if (fix.getMatchID() == fixture.getMatchID()) {
							registeredPlayers.add(plyer);
						}
					}
				}

				map.addAttribute("selectedplayers", selectedPlayers);
				map.addAttribute("fixture", fixture);
				map.addAttribute("registeredplayers", registeredPlayers);
				map.addAttribute("team", teamName);
				return "select-squad";
			}
		}

		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value="/coach/readmessage", method = RequestMethod.GET)
	public String readMessage(HttpServletRequest request, UserDAO userDAO, FixtureDAO fixtureDAO,
			MessageDAO messageDAO, ModelMap map) throws Exception {
		int messageID = Integer.valueOf(request.getParameter("message"));
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		try {
		Coach coach = (Coach) user.getPerson();
		if  (coach instanceof Coach) {
			User intializedUser = userDAO.InitializeUser(user);
			Team team = coach.getTeam();
			ArrayList<Fixture> fixtures = fixtureDAO.getFixtures(team);
			
		
		Message message = messageDAO.getMessage(messageID);	
		message.setStatus("Read");
		Message m = messageDAO.updateMessage(message);
		ArrayList<Message> messages = messageDAO.getMessages(intializedUser);
		map.addAttribute("fixtures", fixtures);
		map.addAttribute("team", team);
		map.addAttribute("user", intializedUser);
		map.addAttribute("messages", messages);
		return "coach-dashboard";
		} 
		}
		catch(HibernateException e) {
			System.out.println("Error: "+e.getMessage());
		}
		return null;
	}

}

