	package com.me.finalproject.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.me.finalproject.filter.RequestWrapper;

public class Sanitizer implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		System.out.println("inside filter class");
		HttpServletRequest modifiedReq= new RequestWrapper((HttpServletRequest) request);
		chain.doFilter(modifiedReq,response);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
