package com.me.finalproject.pojo;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="Player")
@PrimaryKeyJoinColumn(name="playerID", referencedColumnName = "PersonnelID")
public class Player extends Personnel{

	@Column(name = "squadNumber")
	private int squadNumber;
	
	@Column(name = "position")
	private String position;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="teamID", nullable = false)
	private Team team;
	
	@OneToMany(fetch = FetchType.EAGER)
	 @JoinTable(name = "player_fixture", 
			 joinColumns = @JoinColumn(name = "playerID"), inverseJoinColumns = 
			 @JoinColumn(name = "fixtureID"))
	private Set<Fixture> fixtures;
	
	@OneToMany(fetch = FetchType.EAGER)
	 @JoinTable(name = "player_selected_fixture", 
			 joinColumns = @JoinColumn(name = "playerID"), inverseJoinColumns = 
			 @JoinColumn(name = "fixtureID"))
	private Set<Fixture> selectedFixtures;
	
	public int getSquadNumber() {
		return squadNumber;
	}
	public void setSquadNumber(int squadNumber) {
		this.squadNumber = squadNumber;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public Team getTeam() {
		return team;
	}
	public void setTeam(Team team) {
		this.team = team;
	}
	public Set<Fixture> getFixtures() {
		return fixtures;
	}
	public void setFixtures(Set<Fixture> fixtures) {
		this.fixtures = fixtures;
	}
	public Set<Fixture> getSelectedFixtures() {
		return selectedFixtures;
	}
	public void setSelectedFixtures(Set<Fixture> selectedFixtures) {
		this.selectedFixtures = selectedFixtures;
	}
	
	
	
	
}
