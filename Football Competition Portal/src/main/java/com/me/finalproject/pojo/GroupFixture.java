package com.me.finalproject.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="GroupFixture")
@PrimaryKeyJoinColumn(name="fixtureID", referencedColumnName = "fixtureID")
public class GroupFixture extends Fixture{
	
	@Column(name="grp")
	private String group;

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}
	
	
}
