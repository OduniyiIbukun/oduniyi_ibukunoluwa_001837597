package com.me.finalproject.pojo;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name= "Fixture")
public class Fixture {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="fixtureID", unique = true, nullable = false)
	private int matchID;
	
	@Column(name="matchType")
	private String matchType;
	
	@Temporal(TemporalType.DATE)
	@Column(name="fixtureDate")
	private Date fixtureDate;
	
	@OneToOne
	@JoinColumn(name = "team1")
	private Team team1;
	
	@OneToOne
	@JoinColumn(name = "team2")
	private Team team2;
	
	@OneToOne
	@JoinColumn(name="stadium")
	private Stadium stadium;
	
	@OneToOne
	@JoinColumn(name="referee")
	private Referee ref;
	
	@Column(name = "attendance")
	private int attendance;
	
	@OneToMany(mappedBy="team",fetch = FetchType.LAZY)
	private Set<Event> events;
	
	@Column
	private String status;
	
	public int getMatchID() {
		return matchID;
	}
	public void setMatchID(int matchID) {
		this.matchID = matchID;
	}
	public String getMatchType() {
		return matchType;
	}
	public void setMatchType(String matchType) {
		this.matchType = matchType;
	}
	public Date getFixtureDate() {
		return fixtureDate;
	}
	public void setFixtureDate(Date fixtureDate) {
		this.fixtureDate = fixtureDate;
	}
	public Team getTeam1() {
		return team1;
	}
	public void setTeam1(Team team1) {
		this.team1 = team1;
	}
	public Team getTeam2() {
		return team2;
	}
	public void setTeam2(Team team2) {
		this.team2 = team2;
	}
	public Stadium getStadium() {
		return stadium;
	}
	public void setStadium(Stadium stadium) {
		this.stadium = stadium;
	}
	public Referee getRef() {
		return ref;
	}
	public void setRef(Referee ref) {
		this.ref = ref;
	}
	public int getAttendance() {
		return attendance;
	}
	public void setAttendance(int attendance) {
		this.attendance = attendance;
	}
	public Set<Event> getEvents() {
		return events;
	}
	public void setEvents(Set<Event> events) {
		this.events = events;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
}
