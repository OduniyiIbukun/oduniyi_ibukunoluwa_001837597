package com.me.finalproject.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="KnockoutFixture")
@PrimaryKeyJoinColumn(name="fixtureID", referencedColumnName = "fixtureID")
public class KnockoutFixture extends Fixture{
	
	@Column(name="knockoutStage")
	private String knockoutStage;

	public String getKnockoutStage() {
		return knockoutStage;
	}

	public void setKnockoutStage(String knockoutStage) {
		this.knockoutStage = knockoutStage;
	}
	
	
}
