package com.me.finalproject.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="Coach")
@PrimaryKeyJoinColumn(name="coachID", referencedColumnName = "PersonnelID")
public class Coach extends Personnel {

	@Column(name="coachQualification")
	private String coachQualification;
	@Column(name = "Style")
	private String coachStyle;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "teamID")
	private Team team;
	

	public String getCoachQualification() {
		return coachQualification;
	}

	public void setCoachQualification(String coachQualification) {
		this.coachQualification = coachQualification;
	}

	public String getCoachStyle() {
		return coachStyle;
	}

	public void setCoachStyle(String coachStyle) {
		this.coachStyle = coachStyle;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}
	
	
	
}
