package com.me.finalproject.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "Referee")
@PrimaryKeyJoinColumn(name = "refereeID", referencedColumnName = "PersonnelID")
public class Referee extends Personnel {

	@Column(name = "refClass")
	private String refClass;

	@Column(name = "nationality")
	private String Nationality;

	public String getRefClass() {
		return refClass;
	}

	public void setRefClass(String refClass) {
		this.refClass = refClass;
	}

	public String getNationality() {
		return Nationality;
	}

	public void setNationality(String nationality) {
		Nationality = nationality;
	}

	
	
}
