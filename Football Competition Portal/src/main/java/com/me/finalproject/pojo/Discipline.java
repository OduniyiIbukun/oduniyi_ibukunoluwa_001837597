package com.me.finalproject.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="Discipline")
@PrimaryKeyJoinColumn(name="foulID", referencedColumnName = "eventID")
public class Discipline extends Event {

	@Column(name = "description")
	private String description;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="playerID")
	private Player player;
	
	@Column(name = "card")
	private String card;
	
	
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Player getPlayer() {
		return player;
	}
	public void setPlayer(Player player) {
		this.player = player;
	}
	public String getCard() {
		return card;
	}
	public void setCard(String card) {
		this.card = card;
	}	
	
	
}
