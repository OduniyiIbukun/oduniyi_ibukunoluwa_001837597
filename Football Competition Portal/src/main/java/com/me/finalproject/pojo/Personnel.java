package com.me.finalproject.pojo;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "Personnel")
public class Personnel {

	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="personnelID", unique = true, nullable = false)
	private int personnelID;
	
	@Column(name= "firstName")
	private String firstName;
	
	@Column(name= "lastName")
	private String lastName;
	
	@Temporal(TemporalType.DATE)
	@Column(name= "Birthdate")
	private Date Birthdate;
	
	@Column(name= "category")
	private String category;
	
	
	 @OneToOne(cascade = CascadeType.ALL, mappedBy = "person", fetch = FetchType.LAZY)
	private User user;
	 
		@OneToMany
		 @JoinTable(name = "user_message", 
				 joinColumns = @JoinColumn(name = "userID"), inverseJoinColumns = 
				 @JoinColumn(name = "messageID"))
		private Set<Message> messages; 
	
	public int getPersonnelID() {
		return personnelID;
	}
	public void setPersonnelID(int personnelID) {
		this.personnelID = personnelID;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getBirthdate() {
		return Birthdate;
	}
	public void setBirthdate(Date birthdate) {
		Birthdate = birthdate;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	
	
	
}
