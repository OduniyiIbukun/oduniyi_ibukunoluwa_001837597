package com.me.finalproject.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Stadium")
public class Stadium {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="stadiumID", unique = true, nullable = false)
	private int stadiumID;
	
	@Column(name = "stadiumName")
	private String stadiumName;
	
	@Column(name = "stadiumLocation")
	private String stadiumLocation;
	
	@Column(name = "capacity")
	private int capacity;
	
	public int getStadiumID() {
		return stadiumID;
	}
	public void setStadiumID(int stadiumID) {
		this.stadiumID = stadiumID;
	}
	public String getStadiumName() {
		return stadiumName;
	}
	public void setStadiumName(String stadiumName) {
		this.stadiumName = stadiumName;
	}
	public String getStadiumLocation() {
		return stadiumLocation;
	}
	public void setStadiumLocation(String stadiumLocation) {
		this.stadiumLocation = stadiumLocation;
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	
	
}
