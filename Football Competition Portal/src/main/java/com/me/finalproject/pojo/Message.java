package com.me.finalproject.pojo;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Controller;

@Entity
@Table(name = "Message")
public class Message {

	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="messageID", unique = true, nullable = false)
	private int messageID;
	

	@ManyToOne(cascade = CascadeType.ALL)
	private User receiver;
	

	@ManyToOne(cascade = CascadeType.ALL)
	private User sender;
	
	@Column(name = "content")
	private String content;
	
	@Column(name = "messageDate")
	private Date messageDate;
	
	@Column(name = "status")
	private String status;


	public User getReceiver() {
		return receiver;
	}

	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}

	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getMessageDate() {
		return messageDate;
	}

	public void setMessageDate(Date messageDate) {
		this.messageDate = messageDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getMessageID() {
		return messageID;
	}

	public void setMessageID(int messageID) {
		this.messageID = messageID;
	}


	
	
	
}
