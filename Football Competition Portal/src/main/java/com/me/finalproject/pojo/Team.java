package com.me.finalproject.pojo;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Team")
public class Team {

	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="teamID", unique = true, nullable = false)
	private int teamID;
	
	@Column(name = "teamName")
	private String teamName;
	
	@Column(name = "grp")
	String group;
	
	@OneToMany(mappedBy="team",fetch = FetchType.EAGER)
	private Set<Player> players;
	
	@OneToOne(mappedBy = "team", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Coach coach;
	
	public Team() {}
	
	public int getTeamID() {
		return teamID;
	}
	public void setTeamId(int teamID) {
		this.teamID = teamID;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public Set<Player> getPlayers() {
		return players;
	}
	public void setPlayers(Set<Player> players) {
		this.players = players;
	}
	public Coach getCoach() {
		return coach;
	}
	public void setCoach(Coach coach) {
		this.coach = coach;
	}
	
	
	
	
	
}
