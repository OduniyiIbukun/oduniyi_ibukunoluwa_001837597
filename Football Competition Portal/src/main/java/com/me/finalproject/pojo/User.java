package com.me.finalproject.pojo;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private long id;

	@Column(name = "userEmail")
	private String userEmail;

	@Column(name = "password")
	private String password;

	@Column(name = "status")
	private int status;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "personID")
	private Personnel person;
	
	@OneToMany(fetch = FetchType.EAGER)
	 @JoinTable(name = "user_message", 
			 joinColumns = @JoinColumn(name = "userID"), inverseJoinColumns = 
			 @JoinColumn(name = "messageID"))
	private Set<Message> messages; 
	

	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Set<Message> getMessages() {
		return messages;
	}

	public void setMessages(Set<Message> messages) {
		this.messages = messages;
	}

	public User() {

	}

	public Personnel getPerson() {
		return person;
	}

	public void setPerson(Personnel person) {
		this.person = person;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}