/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author mubarakibukunoluwa
 */
public class Business {
    private String name;
    private PersonDirectory personDirectory;
    private UserDirectory userDirectory;

    public Business(String n){
      name = n;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    public PersonDirectory getPersonDirectory() {
        return personDirectory;
    }

    public void setPersonDirectory(PersonDirectory personDirectory) {
        this.personDirectory = personDirectory;
    }

    public UserDirectory getUserDirectory() {
        return userDirectory;
    }
    

    public void setUserDirectory(UserDirectory userDirectory) {
        this.userDirectory = userDirectory;
    }
    
    /*public void addToUserDirectory(User user ){
    userDirectory.add(user);
    }*/
    
}
