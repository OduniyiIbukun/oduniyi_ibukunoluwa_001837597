/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class UserDirectory {
     private ArrayList<User> users;

     public UserDirectory(){
     users = new ArrayList();
     }
     
    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }
    
    public void addUsers(User user){
    users.add(user);
    }
    
    public void deleteUsers(User user){
    users.remove(user);
    }
     
    public User isValidUser(String userID, String password){
        User FoundUser = new User();
        
    for(User eachUser : users){
        if(userID.equals(eachUser.getUserID()) && password.equals(eachUser.getPassword())){
            FoundUser = eachUser;
        }
       }
    
    return FoundUser;
    }
     
}
