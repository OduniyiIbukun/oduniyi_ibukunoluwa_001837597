/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author mubarakibukunoluwa
 */


public class ConfigureAbusiness {
    
    
    public static Business Initialize (String n){
    Business defaultBusiness = new Business(n); 
    UserDirectory users = new UserDirectory();
    PersonDirectory employees = new PersonDirectory();
    
    Person employee1 = new Person();
    employee1.setFirstName("Tony");
    employee1.setLastName("John");
   
    User user1 = new User();
    user1.setUserID("TJ_1995");
    user1.setPassword("Tony1995John");
    user1.setRole("System Admin");
    user1.setStatus("Active");
    user1.setPerson(employee1);
    
     Person HR = new Person();
    HR.setFirstName("Eden");
    HR.setLastName("Hazard");
   
    User user2 = new User();
    user2.setUserID("EdenBoy");
    user2.setPassword("Hazard2017");
    user2.setRole("Human Resources");
    user2.setStatus("Active");
    user2.setPerson(HR);
    
    users.addUsers(user1);
    users.addUsers(user2);
    employees.addNewPerson(employee1);
    employees.addNewPerson(HR);
    
    defaultBusiness.setPersonDirectory(employees);
    defaultBusiness.setUserDirectory(users);
    /*defaultBusiness.getUserDirectory().addUsers(user1);
    defaultBusiness.getPersonDirectory().addNewPerson(employee1);*/
    //create person and user objects here
    return defaultBusiness;    
    }
    
    
}
