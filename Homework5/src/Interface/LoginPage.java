/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.Business;
import Business.ConfigureAbusiness;
import Business.User;
import Interface.ManageAccountDirectory.SystemAdminWorkArea;
import Interface.ManagePersonDirectory.HRWorkArea;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author mubarakibukunoluwa
 */
public class LoginPage extends javax.swing.JPanel {

    /**
     * Creates new form LoginPage
     */
     private JPanel UserProcessPanel;
     private Business defaultBusiness;
     private User user;
     private User userAdmin;
     
    LoginPage(JPanel UserProcessPanel, Business defaultBusiness) {
         initComponents();
         this.UserProcessPanel = UserProcessPanel;
         this.defaultBusiness = defaultBusiness;
         user = new User();
         /*user = defaultBusiness.getUserDirectory().getUsers().get(1);
         userNameTxt.setText(user.getUserID());
         passwordTxt.setText(user.getPassword());*/
    }
    
     

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        userNameTxt = new javax.swing.JTextField();
        passwordTxt = new javax.swing.JTextField();
        loginBtn = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();

        jLabel1.setText("User Name:");

        jLabel2.setText("Password:");

        loginBtn.setText("Login");
        loginBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loginBtnActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel3.setText("User Login");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1))
                .addGap(47, 47, 47)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(loginBtn)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(userNameTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                        .addComponent(passwordTxt)))
                .addContainerGap(30, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(userNameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(passwordTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(36, 36, 36)
                .addComponent(loginBtn)
                .addContainerGap(73, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void loginBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loginBtnActionPerformed
        // TODO add your handling code here:
       user.setUserID(userNameTxt.getText());
       user.setPassword(passwordTxt.getText());
       User MatchUser;
      MatchUser = defaultBusiness.getUserDirectory().isValidUser(user.getUserID(), user.getPassword());
       if(MatchUser.getStatus().equals("Disabled")){
       JOptionPane.showMessageDialog(null, "This account has been Disabled!");
       }
       
      if((MatchUser.getRole().equals("System Admin")) && (MatchUser.getStatus().equals("Active"))){
    SystemAdminWorkArea adminWorkArea  = new SystemAdminWorkArea(UserProcessPanel, defaultBusiness, MatchUser);
        UserProcessPanel.add("SystemAdminWorkArea",adminWorkArea);
        CardLayout layout = (CardLayout) UserProcessPanel.getLayout();  
        layout.next(UserProcessPanel);
     }
      
      else if((MatchUser.getRole().equals("Human Resources")) && (MatchUser.getStatus().equals("Active"))){
        HRWorkArea hrWorkArea = new HRWorkArea(UserProcessPanel, defaultBusiness,MatchUser);
        UserProcessPanel.add("HRWorkArea", hrWorkArea);
        CardLayout layout = (CardLayout) UserProcessPanel.getLayout();  
        layout.next(UserProcessPanel);
     }
      else if(MatchUser.equals(null)){
      JOptionPane.showMessageDialog(null, "This account doesn't exist!");
      }
      
     
       
       
     /*for(User users : defaultBusiness.getUserDirectory().getUsers()){
       if(user.getUserID().equals(users.getUserID()) && user.getPassword().equals(users.getPassword())){
          if(user.getRole().equals("System Admin")){
          SystemAdminWorkArea adminWorkArea  = new SystemAdminWorkArea(UserProcessPanel, defaultBusiness, users);
        UserProcessPanel.add("SystemAdminWorkArea",adminWorkArea);
        CardLayout layout = (CardLayout) UserProcessPanel.getLayout();  
        layout.next(UserProcessPanel);
          }
        else if(user.getRole().equals("Human Resources")){
        HRWorkArea hrWorkArea = new HRWorkArea(UserProcessPanel, defaultBusiness, users);
        UserProcessPanel.add("HRWorkArea", hrWorkArea);
        CardLayout layout = (CardLayout) UserProcessPanel.getLayout();  
        layout.next(UserProcessPanel);
                }
        else{
        JOptionPane.showMessageDialog(null, "You have successfully logged in");
        }
          
          
          }
       
       else{
       JOptionPane.showMessageDialog(null, "This account doesn't exist!");
       }
       }*/
     
     
     
    }//GEN-LAST:event_loginBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JButton loginBtn;
    private javax.swing.JTextField passwordTxt;
    private javax.swing.JTextField userNameTxt;
    // End of variables declaration//GEN-END:variables
}
