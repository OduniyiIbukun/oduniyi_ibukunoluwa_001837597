/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class CustomerDirectory {
    
    private ArrayList<Customer> Customers;

    public CustomerDirectory(){
    Customers = new ArrayList<Customer>();
    }
            
    public ArrayList<Customer> getCustomers() {
        return Customers;
    }

    public void setCustomers(ArrayList<Customer> Customers) {
        this.Customers = Customers;
    }
    
    public void add(Customer customer){
       Customers.add(customer);
    }
    
}
