/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class AirlineDirectory {
    
    private ArrayList<Airline> airlines;
    
    public AirlineDirectory(){
    airlines = new ArrayList<Airline>();
    }

    public ArrayList<Airline> getAirlines() {
        return airlines;
    }

    public void setAirlines(ArrayList<Airline> airlines) {
        this.airlines = airlines;
    }

    public void add(Airline airline) {
        airlines.add(airline);
    }
    
    public void deleteAirline(Airline airline){
    airlines.remove(airline);
    }
    
    
}
