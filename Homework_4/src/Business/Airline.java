/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class Airline {
 
   private String AirlineName;
   private String AirlineNumber;
   private ArrayList<Flight> flights;
   
   public Airline(){
   flights =new ArrayList<Flight>() ;
   }

    public String getAirlineName() {
        return AirlineName;
    }

    public void setAirlineName(String AirlineName) {
        this.AirlineName = AirlineName;
    }

    public String getAirlineNumber() {
        return AirlineNumber;
    }

    public void setAirlineNumber(String AirlineNumber) {
        this.AirlineNumber = AirlineNumber;
    }

    public ArrayList<Flight> getFlights() {
        return flights;
    }

    public void setFlights(ArrayList<Flight> flights) {
        this.flights = flights;
    }
   
   public String toString(){
   return this.AirlineName;
   }
    
    public void add(Flight flight){
       flights.add(flight);
    }
    
}
