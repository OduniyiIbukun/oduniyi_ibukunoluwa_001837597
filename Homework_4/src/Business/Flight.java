/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mubarakibukunoluwa
 */
public class Flight {
   
    private String flightNumber;
    private String destination;
    private String depature;
    private String timeOfDay;
    private String AirlineName;
    private String AirlineNumber;
    private ArrayList<Customer> flyers;
    private ArrayList<Seats> seats;
    
    
     public Flight(){
     seats = new ArrayList<Seats>();
        try {
            convertCsvToJava();
        } catch (ParseException ex) {
            Logger.getLogger(Flight.class.getName()).log(Level.SEVERE, null, ex);
        }
     }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDepature() {
        return depature;
    }

    public void setDepature(String depature) {
        this.depature = depature;
    }

    public String getTimeOfDay() {
        return timeOfDay;
    }

    public void setTimeOfDay(String timeOfDay) {
        this.timeOfDay = timeOfDay;
    }

    public String getAirlineNumber() {
        return AirlineNumber;
    }

    public void setAirlineNumber(String AirlineNumber) {
        this.AirlineNumber = AirlineNumber;
    }

    public String getAirlineName() {
        return AirlineName;
    }

    public void setAirlineName(String AirlineName) {
        this.AirlineName = AirlineName;
    }

    public ArrayList<Customer> getFlyers() {
        return flyers;
    }

    public void setFlyers(ArrayList<Customer> flyers) {
        this.flyers = flyers;
    }

    public ArrayList<Seats> getSeats() {
        return seats;
    }

    public void setSeats(ArrayList<Seats> seats) {
        this.seats = seats;
    }

    public String toString(){
   return this.flightNumber;
   }
    
    
     public void add(Seats seat) {
    seats.add(seat);  
    } 
     
     public int numberOfSeats(){
     int numberOfSeats = seats.size();
     return numberOfSeats;
     }
     
     public void convertCsvToJava() throws ParseException { 
        
  String csvFileToRead = "/Users/mubarakibukunoluwa/Documents/seats.csv";
  BufferedReader br = null;  
  String line = "";  
  String splitBy = ",";  
  
  try {  
  
   br = new BufferedReader(new FileReader(csvFileToRead));  
   while ((line = br.readLine()) != null) {  
  
    // split on comma(',')  
    String[] seatField = line.split(splitBy);  
  
    // create seat object object to store values  
   
     Seats seat = new Seats();
  
    // add values from csv to flight object  
    seat.setSeatNum(seatField[0]);  
    seat.setSeatColumn(seatField[1]);
    seat.setSeatRow(seatField[2]);
  
    // adding seat to a list  
    seats.add(seat);
  
   }  
   
  } catch (FileNotFoundException e) {  
   e.printStackTrace();  
  } catch (IOException e) {  
   e.printStackTrace();  
  } finally {  
   if (br != null) {  
    try {  
     br.close();  
    } catch (IOException e) {  
     e.printStackTrace();  
    }  
   }  
  }
    }
     
}
