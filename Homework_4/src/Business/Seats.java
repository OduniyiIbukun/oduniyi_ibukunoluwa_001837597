/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author mubarakibukunoluwa
 */
public class Seats {
    
    
        String seatNum;
        String seatRow;
        String seatColumn;
        int price;
        String seatStatus;
        
        public Seats() {
            seatStatus = "available";
        }
        
    public Seats(String seatNum, String column, String seatRow){
    this.seatNum = seatNum;
    this.seatColumn = column;
    this.seatRow = seatRow;
    seatStatus = "available";
    }

    public String getSeatColumn() {
        return seatColumn;
    }

    public void setSeatColumn(String seatColumn) {
        this.seatColumn = seatColumn;
    }
    
    

    
    public String getSeatNum() {
        return seatNum;
    }

    public void setSeatNum(String seatNum) {
        this.seatNum = seatNum;
    }

    public String getSeatRow() {
        return seatRow;
    }

    public void setSeatRow(String seatRow) {
        this.seatRow = seatRow;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    
   public void priceDeterminant(){
   if(seatRow == "W"){
   price = 800;
   }
   else 
       price = 600;
   }

    public String getSeatStatus() {
        return seatStatus;
    }

    public void setSeatStatus(String seatStatus) {
        this.seatStatus = seatStatus;
    }
   
    
    
}
