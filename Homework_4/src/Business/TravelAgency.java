/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class TravelAgency {
   private AirlineDirectory airliners;
   private CustomerDirectory cd;

    public AirlineDirectory getAirliners() {
        return airliners;
    }

    public void setAirliners(AirlineDirectory airliners) {
        this.airliners = airliners;
    }

    public CustomerDirectory getCd() {
        return cd;
    }

    public void setCd(CustomerDirectory cd) {
        this.cd = cd;
    }

    
    
   
}
