/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.Airline;
import Business.AirlineDirectory;
import java.awt.CardLayout;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JPanel;

/**
 *
 * @author mubarakibukunoluwa
 */
public class AirlinerWorkArea extends javax.swing.JPanel {

    /**
     * Creates new form FlightWorkArea
     */
    
   private JPanel UserProcessPanel;
   private AirlineDirectory airlineList;
           

    AirlinerWorkArea(JPanel UserProcessPanel, AirlineDirectory airlineList) {
         initComponents();
         this.UserProcessPanel = UserProcessPanel;
         this.airlineList = airlineList;
    }
    
    
    
   
   
   

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        listAirlinerBtn = new javax.swing.JButton();
        flightSearchBtn = new javax.swing.JButton();

        listAirlinerBtn.setText("List All Airliners");
        listAirlinerBtn.setMaximumSize(new java.awt.Dimension(100, 30));
        listAirlinerBtn.setMinimumSize(new java.awt.Dimension(100, 30));
        listAirlinerBtn.setPreferredSize(new java.awt.Dimension(100, 30));
        listAirlinerBtn.setSize(new java.awt.Dimension(100, 30));
        listAirlinerBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listAirlinerBtnActionPerformed(evt);
            }
        });

        flightSearchBtn.setText("Flight Search");
        flightSearchBtn.setPreferredSize(new java.awt.Dimension(100, 30));
        flightSearchBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                flightSearchBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(listAirlinerBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 588, Short.MAX_VALUE)
                    .addComponent(flightSearchBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(87, 87, 87)
                .addComponent(listAirlinerBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(flightSearchBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(359, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void listAirlinerBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listAirlinerBtnActionPerformed
       
        ListAllAirliners panel = new ListAllAirliners(UserProcessPanel, airlineList);
        UserProcessPanel.add("ListAllAirliners", panel);
        CardLayout layout = (CardLayout) UserProcessPanel.getLayout();  
        layout.next(UserProcessPanel);
    }//GEN-LAST:event_listAirlinerBtnActionPerformed

    private void flightSearchBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_flightSearchBtnActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_flightSearchBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton flightSearchBtn;
    private javax.swing.JButton listAirlinerBtn;
    // End of variables declaration//GEN-END:variables
}
