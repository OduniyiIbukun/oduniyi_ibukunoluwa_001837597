/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Portal;


/**
 *
 * @author mubarakibukunoluwa
 */
public class Person {
    public String Fname;
    public String Lname;
    public String nationality;
    public Address address;
    public CreditCard creditCard;
    public Account checkingAccount;
    public Account savingsAccount;
    public License license;
    public String DOB; 

    public void creditCard(){
    
    }
    public String getDOB() {
        return DOB;
    }

   public void setDOB(String DOB) {
        this.DOB = DOB;
    } 
    
    public Person(){
    address = new Address();
    creditCard = new CreditCard();
    savingsAccount = new Account();
    checkingAccount = new Account();
    license = new License();
    }
    
    public String getFname() {
        return Fname;
    }

    public void setFname(String Fname) {
        this.Fname = Fname;
    }

    public String getLname() {
        return Lname;
    }

    public void setLname(String Lname) {
        this.Lname = Lname;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }
    
     public Address getAddress(){
        return address;
    }
     
     public CreditCard getCreditCard(){
     return creditCard;
     }
     
    public Account getSavingsAccount(){
    return savingsAccount;
    }
    
    public Account getCheckingAccount(){
    return checkingAccount;
    }
    
    public License getLicense(){
    return license;
    }
}
