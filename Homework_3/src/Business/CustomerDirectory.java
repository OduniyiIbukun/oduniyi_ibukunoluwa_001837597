/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class CustomerDirectory {
    
    ArrayList<Customer> customerDirectory;
    
    public CustomerDirectory(){
customerDirectory = new ArrayList<>();
}

    public ArrayList<Customer> getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(ArrayList<Customer> customer) {
        this.customerDirectory = customer;
    }
    
    public void add(Customer customer) {
    customerDirectory.add(customer);  
    } 
    
    public Customer addToDirectory(){
    Customer c = new Customer();
    customerDirectory.add(c);
    return c;
    }
    
}
