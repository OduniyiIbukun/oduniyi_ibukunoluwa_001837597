/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class Customer {
  private String Name;
  private int Number;
  private String destination;
   Seat seats;
  
  public Customer(String name,int number,String destination){
  this.Name = name;
  this.Number = number;
  this.destination = destination;
  }

    public Customer() {
        
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getNumber() {
        return Number;
    }

    public void setNumber(int Number) {
        this.Number = Number;
    }
    
    public void setSeat(Seat seat){
    this.seats = seat;
    }

   /* public ArrayList<Seat> getSeats() {
        return seats;
    }

    public void setSeats(ArrayList<Seat> seats) {
        this.seats = seats;
    }
    
    public void add(Seat seat){
    seats.add(seat);
    }*/

    public Seat getSeats() {
        return seats;
    }
    
    
  
}
