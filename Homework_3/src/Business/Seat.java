/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author mubarakibukunoluwa
 */
public class Seat {
    int seatNum;
    String seatRow;
    int seatColumn;
    int price;
    String seatStatus;
    
   public Seat(){
    
    }
    
    public Seat(int seatNum, int column, String seatRow){
    this.seatNum = seatNum;
    this.seatColumn = column;
    this.seatRow = seatRow;
    }

    public int getSeatColumn() {
        return seatColumn;
    }

    public void setSeatColumn(int seatColumn) {
        this.seatColumn = seatColumn;
    }
    
    

    
    public int getSeatNum() {
        return seatNum;
    }

    public void setSeatNum(int seatNum) {
        this.seatNum = seatNum;
    }

    public String getSeatRow() {
        return seatRow;
    }

    public void setSeatRow(String seatRow) {
        this.seatRow = seatRow;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    
   public void priceDeterminant(){
   if(seatRow == "W"){
   price = 800;
   }
   else 
       price = 600;
   }
   
    
}
