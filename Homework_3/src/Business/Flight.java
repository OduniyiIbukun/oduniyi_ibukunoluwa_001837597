/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class Flight {
    
    int  flightNumber;
    ArrayList<Seat> seats;
    String destination;
    
    public Flight(){
    seats = new ArrayList<Seat>();
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(int flightNumber) 
    {
        this.flightNumber = flightNumber;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public ArrayList<Seat> getSeats() {
        return seats;
    }

    public void setSeats(ArrayList<Seat> seats) {
        this.seats = seats;
    }
    
    
      
   public void add(Seat seat) {
    seats.add(seat);  
    } 
   
}
