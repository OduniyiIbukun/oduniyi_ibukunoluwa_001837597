/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class Airliners {
    
    String AirlinerName;
    ArrayList<Airplane> fleet;
    
    public Airliners(String Name){
    fleet = new ArrayList<Airplane>();
    }

    public String getAirlinerName() {
        return AirlinerName;
    }

    public void setAirlinerName(String AirlinerName) {
        this.AirlinerName = AirlinerName;
    }

    public ArrayList<Airplane> getFleet() {
        return fleet;
    }

    public void setFleet(ArrayList<Airplane> fleet) {
        this.fleet = fleet;
    }
    
    public void add(Airplane plane) {
    fleet.add(plane);
    }
}
