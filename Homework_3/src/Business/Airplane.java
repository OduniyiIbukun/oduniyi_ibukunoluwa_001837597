/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author mubarakibukunoluwa
 */
public class Airplane {
   
    private String airliner;
    private int numOfSeats;
    private int serialNum;
    private String manufacturer;
    private int modelNum; 
    ArrayList<Flight> flights;
    
    
    
    public Airplane(String Manu, int modelNum, int serialNum, int numberOfseats){
    this.numOfSeats = numberOfseats;
    this.serialNum = serialNum;
    this.manufacturer = Manu;
    this.modelNum = modelNum;
    flights = new ArrayList<>();
    }

    public ArrayList<Flight> getFlights() {
        return flights;
    }

    public void setFlights(ArrayList<Flight> flights) {
        this.flights = flights;
    }

    
    
    public int getModelNum() {
        return modelNum;
    }

    public void setModelNum(int modelNum) {
        this.modelNum = modelNum;
    }


    public int getNumOfSeats() {
        return numOfSeats;
    }

    public void setNumOfSeats(int numOfSeats) {
        this.numOfSeats = numOfSeats;
    }

    public int getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(int serialNum) {
        this.serialNum = serialNum;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

        public void add(Flight flight) {
    flights.add(flight);  
    } 
    
   public String toString(){
   return this.manufacturer;
   }
   
   
}

