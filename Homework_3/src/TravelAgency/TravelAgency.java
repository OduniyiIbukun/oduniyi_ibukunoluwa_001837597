/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TravelAgency;
import Business.Airliners;
import Business.Airplane;
import Business.Customer;
import Business.CustomerDirectory;
import Business.Flight;
import Business.Seat;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import javax.swing.JFileChooser;
import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class TravelAgency {
    
    ArrayList<Airliners> travelAgency;
    
    public TravelAgency(){
    travelAgency = new ArrayList<>();
    }
    
    public void addToTravelAgency(Airliners airline){
    travelAgency.add(airline);
}

    public ArrayList<Airliners> getTravelAgency() {
        return travelAgency;
    }

    public void setTravelAgency(ArrayList<Airliners> travelAgency) {
        this.travelAgency = travelAgency;
    }
    
    public static void main(String[] args) {
    

    
     CustomerDirectory cd = new CustomerDirectory();
     
     Customer customer1 = new Customer("John", 1, "Rome");
    
     Customer customer2 = new Customer("Trisha", 2, "Rome");
     Customer customer3 = new Customer("Jerry", 3, "Rome");
     Customer customer4 = new Customer("Tony", 4, "Boston");
     Customer customer5 = new Customer("Ayo", 5, "Boston");
     Customer customer6 = new Customer("Lamar", 6, "Boston");
     Customer customer7 = new Customer("Kroos", 7, "Newyork");
     Customer customer8 = new Customer("Ford", 8, "Newyork");
     Customer customer9 = new Customer("Jeremy", 9, "Newyork");
     Customer customer10 = new Customer("Liam", 10, "Lagos");
     Customer customer11 = new Customer("Shawn", 11, "Lagos");
    Customer customer12 = new Customer("Gerald", 12, "Lagos");
    Customer customer13 = new Customer("Mariam", 13, "Lagos");
    Customer customer14 = new Customer("Lucas",14, "Boston");
    Customer customer15 = new Customer("Adrian",15, "Boston");
    Customer customer16 = new Customer("Demi",16, "Boston");
    Customer customer17 = new Customer("Angela",17, "Boston");
    Customer customer18 = new Customer("Brianna",18, "Rome");
    Customer customer19 = new Customer("Ted",19, "Lagos");
    Customer customer20 = new Customer("Funsho",20,"Rome");
    
    //Adding the customers to the arrayList
    cd.add(customer1);
    cd.add(customer2);
    cd.add(customer3);
    cd.add(customer4);
    cd.add(customer5);
    cd.add(customer6);
    cd.add(customer7);
    cd.add(customer8);
    cd.add(customer9);
    cd.add(customer10);
    cd.add(customer11);
    cd.add(customer12);
    cd.add(customer13);
    cd.add(customer14);
    cd.add(customer15);
    cd.add(customer16);
    cd.add(customer17);
    cd.add(customer18);
    cd.add(customer19);
    cd.add(customer20);
    
    //Flight1 creating new seats 
   Seat seats1 = new Seat(1,1,"W");
   seats1.priceDeterminant();
   
  Seat seats2 = new Seat(2,1,"W");
  seats2.priceDeterminant();
  
  Seat seats3 = new Seat(3,1,"M");
  seats3.priceDeterminant();
          
  Seat seats4 = new Seat(4,1,"W");
  seats4.priceDeterminant();
          
  Seat seats5 = new Seat(1,1,"A");
  seats5.priceDeterminant();
          
  Seat seats6 = new Seat(1,2,"M");
  seats6.priceDeterminant();
  
  Seat seats7 = new Seat(2,1,"M");
  seats7.priceDeterminant();
  
  Seat seats8 = new Seat(6,1,"W");
  seats8.priceDeterminant();
  
  Seat seats9 = new Seat(8,1,"A");
  seats9.priceDeterminant();
  
  Seat seats10 = new Seat(1,2,"A");
  seats10.priceDeterminant();
  
  Seat seats11 = new Seat(21,1,"W");
  seats11.priceDeterminant();
  
  Seat seats12 = new Seat(10,2,"M");
  seats12.priceDeterminant();
  
  Seat seats13 = new Seat(11,1,"A");
  seats13.priceDeterminant();
  
  Seat seats14 = new Seat(11,2,"A");
  seats14.priceDeterminant();
  
  Seat seats15 = new Seat(1,1,"M");
  seats15.priceDeterminant();
  
  Seat seats16 = new Seat(23,1,"W");
  seats16.priceDeterminant();
  
  Seat seats17 = new Seat(24,1,"W");
  seats17.priceDeterminant();
  
  Seat seats18 = new Seat(10,1,"W");
  seats18.priceDeterminant();
  
  Seat seats19 = new Seat(5,1,"M");
  seats19.priceDeterminant();
  
  Seat seats20 = new Seat(1,2,"W");
  seats20.priceDeterminant();
  
  Seat seats21 = new Seat(8,2,"M");
  seats21.priceDeterminant();
  
  Seat seats22 = new Seat(7,1,"W");
  seats22.priceDeterminant();
  
  Seat seats23 = new Seat(1,1,"M");
  seats23.priceDeterminant();
  
  Seat seats24 = new Seat (23,2,"W");
  seats24.priceDeterminant();
  
  Seat seats25 = new Seat (24,1,"W");
  seats25.priceDeterminant();
  
  Seat seats26 = new Seat (24,2,"W");
  seats26.priceDeterminant();
  
  Seat seats27 = new Seat (25,1,"A");
  seats27.priceDeterminant();
  
  //Linking each customer to a seat
   customer1.setSeat(seats1);
   customer2.setSeat(seats23);
   customer3.setSeat(seats19);
   customer4.setSeat(seats4);
   customer5.setSeat(seats5);
   customer6.setSeat(seats6);
   customer7.setSeat(seats7);
   customer8.setSeat(seats8);
   customer9.setSeat(seats9);
   customer10.setSeat(seats16);
   customer11.setSeat(seats14);
   customer12.setSeat(seats20);
   customer13.setSeat(seats22);
   customer14.setSeat(seats17);
   customer15.setSeat(seats18);
   customer16.setSeat(seats26);
   customer17.setSeat(seats27);
   customer18.setSeat(seats2);
   customer19.setSeat(seats3);
   customer20.setSeat(seats10);  
    // adding seat to flights 
    Flight flight1 = new Flight();
    flight1.setFlightNumber(1);
    flight1.setDestination("Rome");
    
     flight1.add(seats1);
     flight1.add(seats2);
     flight1.add(seats3);
     flight1.add(seats4);
     flight1.add(seats5);
     flight1.add(seats6);
     flight1.add(seats7);
     flight1.add(seats8);
     flight1.add(seats9);
     flight1.add(seats10);
     flight1.add(seats11);
     flight1.add(seats12);
     flight1.add(seats13);
     flight1.add(seats14);
     flight1.add(seats15);
     flight1.add(seats16);
     flight1.add(seats17);
     flight1.add(seats18);
     flight1.add(seats19);
     flight1.add(seats20);
     flight1.add(seats21);
     flight1.add(seats22);
     flight1.add(seats23);
     flight1.add(seats24);
     flight1.add(seats25);
     flight1.add(seats26);
     flight1.add(seats27);
     
     Flight flight2 = new Flight();
     flight2.setFlightNumber(2);
     flight2.setDestination("Lagos");
     
     flight2.add(seats23);
     flight2.add(seats22);
     flight2.add(seats21);
     flight2.add(seats20);
     flight2.add(seats19);
     flight2.add(seats18);
     flight2.add(seats17);
     flight2.add(seats16);
     flight2.add(seats15);
     flight2.add(seats14);
     flight2.add(seats13);
     flight2.add(seats12);
     flight2.add(seats11);
     flight2.add(seats10);
     flight2.add(seats9);
     flight2.add(seats8);
     flight2.add(seats7);
     flight2.add(seats6);
     flight2.add(seats5);
     flight2.add(seats4);
     flight2.add(seats3);
     flight2.add(seats2);
     flight2.add(seats1);
     flight2.add(seats24);
     flight2.add(seats25);
     flight2.add(seats26);
     flight2.add(seats27);
    
    
     Flight flight3 = new Flight();
     flight3.setFlightNumber(3);
     flight3.setDestination("Boston");
     
     flight3.add(seats1);
     flight3.add(seats3);
     flight3.add(seats5);
     flight3.add(seats7);
     flight3.add(seats9);
     flight3.add(seats11);
     flight3.add(seats13);
     flight3.add(seats15);
     flight3.add(seats17);
     flight3.add(seats19);
     flight3.add(seats21);
     flight3.add(seats23);
     flight3.add(seats2);
     flight3.add(seats4);
     flight3.add(seats6);
     flight3.add(seats8);
     flight3.add(seats10);
     flight3.add(seats12);
     flight3.add(seats14);
     flight3.add(seats16);
     flight3.add(seats18);
     flight3.add(seats20);
     flight3.add(seats22);
     flight3.add(seats24);
     flight3.add(seats25);
     flight3.add(seats26);
     flight3.add(seats27);
     
     Flight flight4 = new Flight();
     flight4.setFlightNumber(4);
     flight4.setDestination("Newyork");
     
     flight4.add(seats24);
     flight4.add(seats25);
     flight4.add(seats26);
     flight4.add(seats27);
     flight4.add(seats9);
     flight4.add(seats11);
     flight4.add(seats13);
     flight4.add(seats15);
     flight4.add(seats17);
     flight4.add(seats19);
     flight4.add(seats21);
     flight4.add(seats23);
     flight4.add(seats2);
     flight4.add(seats4);
     flight4.add(seats6);
     flight4.add(seats8);
     flight4.add(seats10);
     flight4.add(seats12);
     flight4.add(seats14);
     flight4.add(seats16);
     flight4.add(seats18);
     flight4.add(seats20);
     flight4.add(seats22);
     flight4.add(seats24);
     flight4.add(seats25);
     flight4.add(seats26);
     flight4.add(seats27);
    
     
     //Airplane Objects
     Airplane airplane1 = new Airplane("Airbus",110,101,800);
     airplane1.add(flight1);
    
     Airplane airplane2 = new Airplane("Boeing",777,102,750);
     airplane2.add(flight2);
     
     
     Airplane airplane3 = new Airplane("Boeing",727,108,460);
     airplane3.add(flight3);
     
     Airplane airplane4 = new Airplane("Boeing",777,109,750);
     airplane4.add(flight4);
     
     Airplane airplane5 = new Airplane("Airbus",101,104,360);
     Airplane airplane6 = new Airplane("Airbus",104,110,540);
     Airplane airplane7 = new Airplane("Boeing",747,111,800);
     Airplane airplane8 = new Airplane("Boeing",737,112,500);
     Airplane airplane9 = new Airplane("Boeing",777,115,900);
     Airplane airplane10 = new Airplane("Airbus",111,116,900);
     Airplane airplane11 = new Airplane("Boeing",727,117,460);
     Airplane airplane12 = new Airplane("Airbus",111,118,900);
     
     
     Airliners BritishAirways = new Airliners("British Airways");
     BritishAirways.add(airplane1);
     BritishAirways.add(airplane2);
     
     Airliners Emirates  = new Airliners("Emirates");
     Emirates.add(airplane3);
     Emirates.add(airplane4);
     
     TravelAgency ta = new TravelAgency();
     ta.addToTravelAgency(Emirates);
     ta.addToTravelAgency(BritishAirways);
     
     //Calculating revenue per flight
     int Total = 0;
     for(Seat seat : flight1.getSeats())
     {
      for(Customer customers : cd.getCustomerDirectory())
      {
      if(flight1.getDestination().equals(customers.getDestination()))
      {
          if(seat.equals(customers.getSeats())){
          Total = Total + seat.getPrice();
                  }
      }  
      }
    }
     
     
     System.out.println(" the total revenue from the British Airways flight to Rome " +Total+ " ");
     
//Revenue for second flight
     int Total2 = 0;
     for(Seat seat : flight2.getSeats())
     {
      for(Customer customers : cd.getCustomerDirectory())
      {
      if(flight2.getDestination().equals(customers.getDestination()))
      {
          if(seat.equals(customers.getSeats())){
          Total2 = Total2 + seat.getPrice();
                  }
      }  
      }
    }
     
     System.out.println(" the total revenue from the British Airways flight to Lagos " +Total2+ " ");
     
     //Revenue for the Third flight
     int Total3 = 0;
     for(Seat seat : flight3.getSeats())
     {
      for(Customer customers : cd.getCustomerDirectory())
      {
      if(flight3.getDestination().equals(customers.getDestination()))
      {
          if(seat.equals(customers.getSeats())){
          Total3 = Total3 + seat.getPrice();
                  }
      }  
      }
    }
     
     System.out.println(" the total revenue from the Emirates flight to Boston " +Total3+ " ");
     
     //Revenue for the Forth Flight
     
     int Total4 = 0;
     for(Seat seat : flight4.getSeats())
     {
      for(Customer customers : cd.getCustomerDirectory())
      {
      if(flight4.getDestination().equals(customers.getDestination()))
      {
          if(seat.equals(customers.getSeats())){
          Total4 = Total4 + seat.getPrice();
                  }
      }  
     
    }
}
     System.out.println(" the total revenue from the Emirates flight to Newyork " +Total4+ " ");
     
     
//revenue for British Airways
       int  totalAirlineRevenue1 = 0;
     for(Airplane airplanes : BritishAirways.getFleet()){
     for(Flight flight : airplanes.getFlights()){
        for(Seat seat : flight.getSeats()){
            for(Customer customers: cd.getCustomerDirectory()){
                if(flight.getDestination().equals(customers.getDestination())){
                    if(seat.equals(customers.getSeats())){
                        totalAirlineRevenue1 = totalAirlineRevenue1 + seat.getPrice();
                    }
                }
                
            }
         
         
     }
     }
     
     }
     System.out.println("Total revenue for British Airways is " +totalAirlineRevenue1+ " ");
     
     //revenue for Emirates
     int totalAirlineRevenue2 = 0;
     for(Airplane airplanes : Emirates.getFleet()){
     for(Flight flight : airplanes.getFlights()){
        for(Seat seat : flight.getSeats()){
            for(Customer customers: cd.getCustomerDirectory()){
                if(flight.getDestination().equals(customers.getDestination())){
                    if(seat.equals(customers.getSeats())){
                        totalAirlineRevenue2 = totalAirlineRevenue2 + seat.getPrice();
                    }
                }
                
            }
         
         
     }
     }
     
     }
     
     System.out.println("Total Revenue for emirates is " +totalAirlineRevenue2+ "   ");
     
     
     //Calculating total amount from all Airlines
     int totalRevenue = 0;
     for(Airliners airline : ta.getTravelAgency()){
         for(Airplane planes : airline.getFleet()){
     for(Flight flight : planes.getFlights()){
        for(Seat seat : flight.getSeats()){
            for(Customer customers: cd.getCustomerDirectory()){
                if(flight.getDestination().equals(customers.getDestination())){
                    if(seat.equals(customers.getSeats())){
                        totalRevenue = totalRevenue + seat.getPrice();
                    }
                }
                
            }
         
         
     }
     }
     }
     }    
     
     System.out.println("Total Revenue for every Airliner from every flight is " +totalRevenue+ " ");
    }
}
