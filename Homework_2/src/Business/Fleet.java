/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import java.util.ArrayList;
/**
 *
 * @author mubarakibukunoluwa
 */
public class Fleet {
    
    private ArrayList<Airplanes> fleet;
    private String lastUpdated;

    public Fleet(){
     fleet = new ArrayList<Airplanes>();
    }
    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public ArrayList<Airplanes> getFleet() {
        return fleet;
    }

    public void setFleet(ArrayList<Airplanes> Fleet) {
        this.fleet = Fleet;
    }
    
    public Airplanes addToFleet(){
    Airplanes plane = new Airplanes();
    fleet.add(plane);
    return plane;
    }
    
    

    public void add(Airplanes airplane) {
    fleet.add(airplane);
    }

    public int size() {
         return fleet.size();
    }
    
    
}
