/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author mubarakibukunoluwa
 */
public class Airplanes {
    private String yearMade;
    private int numOfSeats;
    private int serialNum;
    private String manufacturer;
    private String avail;
    private String airport;
    private Date mainCertStatus;
    private int modelNum; 
    private Date time;

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
     
    /*public String getAvail() {
        if (mainCertStatus == "Active"){
        avail = "Available";
        }
        else{
            avail = "Not Available";
        }
        return avail;
    } */

    public void setAvail(String avail) {
        this.avail = avail;
    }

    public int getModelNum() {
        return modelNum;
    }

    public void setModelNum(int modelNum) {
        this.modelNum = modelNum;
    }

    
    
    public String getYearMade() {
        return yearMade;
    }

    public void setYearMade(String yearMade) {
        this.yearMade = yearMade;
    }

    public int getNumOfSeats() {
        return numOfSeats;
    }

    public void setNumOfSeats(int numOfSeats) {
        this.numOfSeats = numOfSeats;
    }

    public int getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(int serialNum) {
        this.serialNum = serialNum;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getavail() {
        return avail;
    }

    public void setTimeOfavail(String timeOfavail) {
        this.avail = timeOfavail;
    }

    public String getAirport() {
        return airport;
    }

    public void setAirport(String airport) {
        this.airport = airport;
    }

    public Date getMainCertStatus() {
        return mainCertStatus;
    }

    public void setMainCertStatus(Date mainCertStatus) {
       this.mainCertStatus = mainCertStatus;
    }
    
   public String toString(){
   return this.manufacturer;
   }
    
}
