/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Organization.AdminOrganization;
import Business.Organization.Organization;
import Business.Organization.Organization.Type;
import Business.Organization.RegulationOrganization;
import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class Phd extends Enterprise{

    public Phd(String name, EnterpriseType Type) {
        super(name, Type.PHD);
    }

    
    @Override
    public ArrayList<Type> getSupportedTypes() {
      ArrayList<Type> organizations = new ArrayList<>();
        organizations.add(Organization.Type.Admin);
        organizations.add(Organization.Type.RegulationOrganization);
        return organizations;  
    }
    
}
