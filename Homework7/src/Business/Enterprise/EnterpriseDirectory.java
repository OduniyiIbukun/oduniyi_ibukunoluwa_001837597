/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Supplier.Supplier;
import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class EnterpriseDirectory {
    
   private ArrayList<Enterprise> enterpriseList; 
   
   public EnterpriseDirectory(){
   enterpriseList = new ArrayList<Enterprise>();
   }

    public ArrayList<Enterprise> getEnterpriseList() {
        return enterpriseList;
    }

    public void setEnterpriseList(ArrayList<Enterprise> enterprises) {
        this.enterpriseList = enterprises;
    }
   
    public Enterprise createAndAddEnterprise(String name, Enterprise.EnterpriseType type){
        Enterprise enterprise = null;
        if (type == Enterprise.EnterpriseType.Hospital){
            enterprise = new Hospital(name, Enterprise.EnterpriseType.Hospital);
            enterpriseList.add(enterprise);
        }
        
        else if (type == Enterprise.EnterpriseType.Distributor) {
        enterprise = new VaccineDistributor(name, Enterprise.EnterpriseType.Distributor);
            enterpriseList.add(enterprise);
        }
        
        else if (type == Enterprise.EnterpriseType.PHD) {
        enterprise = new Phd(name, Enterprise.EnterpriseType.PHD);
            enterpriseList.add(enterprise);
        }
        
        else if (type == Enterprise.EnterpriseType.CDC) {
        enterprise = new Phd(name, Enterprise.EnterpriseType.CDC);
            enterpriseList.add(enterprise);
        }
        
        else if (type == Enterprise.EnterpriseType.Supplier) {
        enterprise = new Supplier(name, Enterprise.EnterpriseType.Supplier);
            enterpriseList.add(enterprise);
        }
        return enterprise;
    }
    
    public void deleteEnterprise(Enterprise e){
    enterpriseList.remove(e);
    }
}
