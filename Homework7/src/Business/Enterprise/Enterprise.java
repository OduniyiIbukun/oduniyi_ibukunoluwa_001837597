/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Organization.Organization;
import Business.Organization.Organization.Type;
import Business.Organization.OrganizationDirectory;
import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public abstract class Enterprise {

    private EnterpriseType enterpriseType;
    private static int counter = 100;
    private String name;
    private OrganizationDirectory organisations;
    private int OrganisationID;

    public abstract ArrayList<Type> getSupportedTypes();

    public enum EnterpriseType {
        Hospital("Hospital") {
        },
        Distributor("Distributor") {
        },
        PHD("Public Health Department") {
        },
        CDC("Center For Disease Control") {
        },
        Supplier("Manufacturer"){};

        private String value;

        private EnterpriseType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }

    public Enterprise(String name, EnterpriseType Type) {
        this.name = name;
        this.enterpriseType = Type;
        organisations = new OrganizationDirectory();
        this.OrganisationID = counter;
        ++counter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

     public OrganizationDirectory getOrganisations() {
        return organisations;
    }

    public void setOrganisations(OrganizationDirectory organisations) {
        this.organisations = organisations;
    }

    public int getOrganisationID() {
        return OrganisationID;
    }

    public void setOrganisationID(int OrganisationID) {
        this.OrganisationID = OrganisationID;
    }

    public EnterpriseType getEnterpriseType() {
        return enterpriseType;
    }

    public void setEnterpriseType(EnterpriseType enterpriseType) {
        this.enterpriseType = enterpriseType;
    }

    @Override
    public String toString(){
    return name;
    }
    
    
}
