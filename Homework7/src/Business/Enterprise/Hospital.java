/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Organization.AdminOrganization;
import Business.Organization.Clinic;
import Business.Organization.Organization;
import Business.Organization.Organization.Type;
import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class Hospital extends Enterprise {

    public Hospital(String name, EnterpriseType Type) {
        super(name, Type.Hospital);
    }

    @Override
    public ArrayList<Type> getSupportedTypes() {
        ArrayList<Type> organizations = new ArrayList<>();
        organizations.add(Type.Admin);
        organizations.add(Type.Clinic);
        return organizations;  
    }
    
}
