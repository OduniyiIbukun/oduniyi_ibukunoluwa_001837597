/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Organization.AdminOrganization;
import Business.Organization.Organization;
import Business.Organization.Organization.Type;
import Business.Organization.Procurement;
import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class VaccineDistributor extends Enterprise{

    public VaccineDistributor(String name, EnterpriseType Type) {
        super(name, Type.Distributor);
    }

    @Override
    public ArrayList<Type> getSupportedTypes(){
        ArrayList<Type> organizations = new ArrayList<>();
        organizations.add(Type.Admin);
        organizations.add(Type.Procurement);
        return organizations;
    }
    
    
    
}
