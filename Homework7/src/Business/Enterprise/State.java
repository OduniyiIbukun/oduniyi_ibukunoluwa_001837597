/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class State  {
    
    private int StateID;
    private String stateName;
    private EnterpriseDirectory enterpriseDirectory;
    private static int counter = 1000;

    public State() {
        enterpriseDirectory = new EnterpriseDirectory();
        this.StateID = counter;
        ++counter;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public EnterpriseDirectory getEnterpriseDirectory() {
        return enterpriseDirectory;
    }

    public void setEnterpriseDirectory(EnterpriseDirectory enterpriseDirectory) {
        this.enterpriseDirectory = enterpriseDirectory;
    }

    public int getStateID() {
        return StateID;
    }

    public void setStateID(int StateID) {
        this.StateID = StateID;
    }

    public String toString(){
    return stateName;
    }
    
    
    
}
