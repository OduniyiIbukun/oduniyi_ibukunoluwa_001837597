/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Disease.DiseaseCatalog;
import Business.Organization.Organization;
import Business.Role.Role;
import Business.Role.SuperUser;
import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class Ecosystem extends Organization{

    
    private static Ecosystem business;
    private ArrayList<Network> networks; 
    private DiseaseCatalog diseases;
    
    private Ecosystem(){
    super(null); 
    networks = new ArrayList<Network>();
    }
    
    public Network CreateNetwork(){
    Network network = new Network();
    networks.add(network);
    return network;
    }
    
    public void deleteNetwork(Network network){
    networks.remove(network);
    }
    
    public static Ecosystem getInstance()
    {
    if(business == null){
    business = new Ecosystem();
    }
    return business;
    }

    public ArrayList<Network> getNetworks() {
        return networks;
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new SuperUser());
        return roles;
    }

    public DiseaseCatalog getDiseases() {
        return diseases;
    }

    public void setDiseases(DiseaseCatalog diseases) {
        this.diseases = diseases;
    }
    
    
    
    public boolean checkIfUsernameIsUnique(String username) {

        if (!this.getUserAccountDirectory().checkIfUsernameIsUnique(username)) {
            return false;
        }

       

        return true;
    }
    
}
