/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Vaccine;

import Business.Disease.Disease;
import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class VaccineCatalog {
 
    private ArrayList<Vaccine> vaccines;
    
    public VaccineCatalog(){
    vaccines = new ArrayList<Vaccine>();
    }

    public ArrayList<Vaccine> getVaccines() {
        return vaccines;
    }

    public void setVaccines(ArrayList<Vaccine> vaccines) {
        this.vaccines = vaccines;
    }
    
    public Vaccine addVaccine(String name, int quantity, Disease disease){
    Vaccine vaccine = new Vaccine();
    vaccine.setName(name);
    vaccine.setQuantity(quantity);
    vaccine.setDisease(disease);
    vaccines.add(vaccine);
    return vaccine;
    }
    
    
    
}
