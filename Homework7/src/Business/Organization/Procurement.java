/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.ProcurementRole;
import Business.Role.Role;
import Business.Vaccine.Vaccine;
import Business.Vaccine.VaccineCatalog;
import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class Procurement extends Organization{
    
    private VaccineCatalog vaccines;
    
    public Procurement(){
      super(Organization.Type.Procurement.getValue());
      vaccines = new VaccineCatalog();
    }
    
     public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new ProcurementRole());
        return roles;
    }

    public VaccineCatalog getVaccines() {
        return vaccines;
    }

    public void setVaccines(VaccineCatalog vaccines) {
        this.vaccines = vaccines;
    }
     
     
     
}
