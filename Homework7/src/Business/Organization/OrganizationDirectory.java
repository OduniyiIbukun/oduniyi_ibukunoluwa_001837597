/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Organization.Organization.Type;
import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class OrganizationDirectory{
    
    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList<Organization>();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(Type type){
        Organization organization = null;
        if (type.getValue().equals(Type.Clinic.getValue())){
            organization = new Clinic();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.Procurement.getValue())){
            organization = new Procurement();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.RegulationOrganization.getValue())){
            organization = new RegulationOrganization();
            organizationList.add(organization);
        }
        else if(type.getValue().equals(Type.Admin.getValue())){
        organization = new AdminOrganization();
            organizationList.add(organization);
        }
        else if(type.getValue().equals(Type.Inventory.getValue())){
        organization = new Inventory();
            organizationList.add(organization);
        }
        
  
        return organization;
    }
}