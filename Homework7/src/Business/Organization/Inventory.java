/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.InventoryManagement;
import Business.Role.Role;
import Business.Vaccine.Vaccine;
import Business.Vaccine.VaccineCatalog;
import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class Inventory extends Organization{

    private VaccineCatalog vaccines;
    
    public Inventory(){
    super(Organization.Type.Inventory.getValue());
    vaccines = new VaccineCatalog();
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
    ArrayList<Role> roles = new ArrayList<>();
        roles.add(new InventoryManagement());
        return roles;
    }

    public VaccineCatalog getVaccines() {
        return vaccines;
    }

    public void setVaccines(VaccineCatalog vaccines) {
        this.vaccines = vaccines;
    }
    
    
    
}
