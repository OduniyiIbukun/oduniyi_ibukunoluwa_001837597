/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.DoctorRole;
import Business.Role.Role;
import Business.Vaccine.Vaccine;
import Business.Vaccine.VaccineCatalog;
import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class Clinic extends Organization{
    
    private VaccineCatalog vaccines;
    
    
    
    
    public Clinic(){
    super(Organization.Type.Clinic.getValue());
    vaccines = new VaccineCatalog();
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new DoctorRole());
        return roles;
    }

    public VaccineCatalog getVaccines() {
        return vaccines;
    }

    public void setVaccines(VaccineCatalog vaccines) {
        this.vaccines = vaccines;
    }
    
    
    
}
