/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.OrderManagementRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class RegulationOrganization extends Organization {

    public RegulationOrganization() {
        super(Organization.Type.RegulationOrganization.getValue());
    }

    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new OrderManagementRole());
        return roles;
    }
    
}
