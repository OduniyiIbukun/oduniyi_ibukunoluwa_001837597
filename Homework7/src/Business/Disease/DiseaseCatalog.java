/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Disease;

import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class DiseaseCatalog {
 
    private ArrayList<Disease> diseases;
    
    public DiseaseCatalog(){
    diseases = new ArrayList<Disease>();
    }

    public ArrayList<Disease> getDiseases() {
        return diseases;
    }

    public void setDiseases(ArrayList<Disease> diseases) {
        this.diseases = diseases;
    }
    
    public Disease addDisease(String DiseaseName){
    Disease disease = new Disease();
    disease.setName(DiseaseName);
    diseases.add(disease);
        return disease; 
    }
    
}
