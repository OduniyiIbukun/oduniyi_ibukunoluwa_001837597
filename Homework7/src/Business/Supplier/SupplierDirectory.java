/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Supplier;
import Business.Enterprise.Enterprise.EnterpriseType;
import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class SupplierDirectory {
    private ArrayList<Supplier> suppliers;

    public SupplierDirectory(){
     suppliers = new ArrayList<Supplier>();
    }
    
    public ArrayList<Supplier> getSuppliers() {
        return suppliers;
    }

    public void setSuppliers(ArrayList<Supplier> suppliers) {
        this.suppliers = suppliers;
    }
    
    public void deleteSuppliers(Supplier supplier){
    suppliers.remove(supplier);
    }
    
     
}
