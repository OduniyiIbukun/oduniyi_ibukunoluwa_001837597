/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Supplier;

import Business.Enterprise.Enterprise;
import Business.Organization.AdminOrganization;
import Business.Organization.Inventory;
import Business.Organization.Organization;
import Business.Vaccine.Vaccine;
import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class Supplier extends Enterprise{

    public Supplier(String name, EnterpriseType Type) {
        super(name, Type.Supplier);
    }
  
    
    public ArrayList<Organization.Type> getSupportedTypes() {
        ArrayList<Organization.Type> organizations = new ArrayList<>();
        organizations.add(Organization.Type.Admin);
        organizations.add(Organization.Type.Inventory);
        return organizations;  
    }

    /*@Override
    public ArrayList<Organization> getSupportedOrganisations() {
        ArrayList<Organization> organizations = new ArrayList<>();
        organizations.add(new AdminOrganization());
        organizations.add(new Inventory());
        return organizations;
    }*/
    
}
