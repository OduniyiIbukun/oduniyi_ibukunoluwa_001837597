/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Enterprise.EnterpriseDirectory;
import Business.Enterprise.State;
import Business.Supplier.SupplierDirectory;
import java.util.ArrayList;

/**
 *
 * @author mubarakibukunoluwa
 */
public class Network {
    
    private int networkID;
    private static int counter = 1 ;
    private String name;
    private ArrayList<State> states;
    private SupplierDirectory suppliers;
    
    public Network(){
    states = new ArrayList<State>();
    suppliers = new SupplierDirectory();
    networkID = counter;
    ++counter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<State> getStates() {
        return states;
    }

    public void setStates(ArrayList<State> states) {
        this.states = states;
    }
    
    public State addState(){
    State state = new State();
    states.add(state);
    return state;
    }
    
    public void deleteState(State state){
    states.remove(state);
}

    public SupplierDirectory getSuppliers() {
        return suppliers;
    }

    public void setSuppliers(SupplierDirectory suppliers) {
        this.suppliers = suppliers;
    }

    public int getNetworkID() {
        return networkID;
    }

    public void setNetworkID(int networkID) {
        this.networkID = networkID;
    }
    
    
    public String toString(){
    return name;
    }
    
}
