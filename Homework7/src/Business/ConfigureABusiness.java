/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Disease.Disease;
import Business.Disease.DiseaseCatalog;
import Business.Employee.Employee;
import Business.Enterprise.Enterprise;
import Business.Enterprise.EnterpriseDirectory;
import Business.Enterprise.State;
import Business.Organization.Inventory;
import Business.Organization.Organization;
import static Business.Organization.Organization.Type.Inventory;
import Business.Role.AdminRole;
import Business.Role.DoctorRole;
import Business.Role.InventoryManagement;
import Business.Role.OrderManagementRole;
import Business.Role.ProcurementRole;
import static Business.Role.Role.RoleType.OrderManagementRole;
import Business.Role.SuperUser;
import Business.Supplier.Supplier;
import Business.UserAccount.UserAccount;
import Business.Vaccine.Vaccine;
import Business.Vaccine.VaccineCatalog;

/**
 *
 * @author mubarakibukunoluwa
 */
public class ConfigureABusiness {

    public static Ecosystem configure() {

        Ecosystem system = Ecosystem.getInstance();

        //Create a network
        Network network = system.CreateNetwork();
        network.setName("HealthCare");

        State state1 = network.addState();
        state1.setStateName("Alabama");

        State state2 = network.addState();
        state2.setStateName("Illinois");

        //Enterprise 1
        Enterprise enterpise1 = state1.getEnterpriseDirectory().createAndAddEnterprise("Saudi German", Enterprise.EnterpriseType.Hospital);
        Organization adminOrg1 = enterpise1.getOrganisations().createOrganization(Organization.Type.Admin);
        Organization doctorOrg = enterpise1.getOrganisations().createOrganization(Organization.Type.Clinic);

        Employee employee = adminOrg1.getEmployeeDirectory().createEmployee("Tony");
        UserAccount account = adminOrg1.getUserAccountDirectory().createUserAccount("Tony", "Tony", employee, new AdminRole());

        Employee employee4 = doctorOrg.getEmployeeDirectory().createEmployee("Tom");
        UserAccount account5 = doctorOrg.getUserAccountDirectory().createUserAccount("Tom", "Tom", employee, new DoctorRole());

        //Enterprise 1 ends
        //Enterprise 2
        Enterprise enterpise2 = state1.getEnterpriseDirectory().createAndAddEnterprise("Alabama Vaccine Distributor", Enterprise.EnterpriseType.Distributor);
        Organization adminOrg2 = enterpise2.getOrganisations().createOrganization(Organization.Type.Admin);
        Organization procurement = enterpise2.getOrganisations().createOrganization(Organization.Type.Procurement);

        Employee employee1 = adminOrg2.getEmployeeDirectory().createEmployee("Mike");
        UserAccount account2 = adminOrg2.getUserAccountDirectory().createUserAccount("Mike", "Mike", employee, new AdminRole());

        Employee employee5 = procurement.getEmployeeDirectory().createEmployee("Jerry");
        UserAccount account6 = procurement.getUserAccountDirectory().createUserAccount("Jerry", "Jerry", employee, new ProcurementRole());

        //Enterprise 2 ends
        //Enterprise 3
        Enterprise enterpise3 = state1.getEnterpriseDirectory().createAndAddEnterprise("Alabama Public Health Department", Enterprise.EnterpriseType.PHD);
        Organization adminOrg3 = enterpise3.getOrganisations().createOrganization(Organization.Type.Admin);
        Organization regOrg = enterpise3.getOrganisations().createOrganization(Organization.Type.RegulationOrganization);

        Employee employee2 = adminOrg3.getEmployeeDirectory().createEmployee("John");
        UserAccount account3 = adminOrg3.getUserAccountDirectory().createUserAccount("John", "John", employee, new AdminRole());

        Employee employee6 = regOrg.getEmployeeDirectory().createEmployee("Jane");
        UserAccount account7 = regOrg.getUserAccountDirectory().createUserAccount("Jane", "Jane", employee, new OrderManagementRole());
        //Enterprise 3 ends

        //Enterprise 4
        Enterprise enterpise4 = state1.getEnterpriseDirectory().createAndAddEnterprise("Swipha", Enterprise.EnterpriseType.Supplier);
        Organization adminOrg4 = enterpise4.getOrganisations().createOrganization(Organization.Type.Admin);
        network.getSuppliers().getSuppliers().add((Supplier) enterpise4);

        Organization inventory = enterpise4.getOrganisations().createOrganization(Organization.Type.Inventory);

        Employee employee3 = adminOrg4.getEmployeeDirectory().createEmployee("Dan");
        UserAccount account4 = adminOrg4.getUserAccountDirectory().createUserAccount("Dan", "Dan", employee, new AdminRole());

        Employee employee7 = inventory.getEmployeeDirectory().createEmployee("Joe");
        UserAccount account8 = inventory.getUserAccountDirectory().createUserAccount("Joe", "Joe", employee, new InventoryManagement());

        //Enterprise 4 ends   
        
        //Vaccines and diseases
        DiseaseCatalog DC = new DiseaseCatalog();
        Disease disease1 = DC.addDisease("Pneumonia");
        Disease disease2 = DC.addDisease("Polio");
        Disease disease3 = DC.addDisease("Hepatitis");
        Disease disease4 = DC.addDisease("Meningitis");
        
        system.setDiseases(DC);

        VaccineCatalog VC = new VaccineCatalog();
        Vaccine vaccine1 = VC.addVaccine("Prevnar", 100, disease1);
        Vaccine vaccine2 = VC.addVaccine("PENTAct-HIB", 100, disease2);
        Vaccine vaccine3 = VC.addVaccine("Pediarix", 100, disease3);
        Vaccine vaccine4 = VC.addVaccine("Menactra", 100, disease4);
        
        inventory.setVaccines(VC);
        procurement.setVaccines(VC);
        doctorOrg.setVaccines(VC);
        
        
        Employee employeeSuper = system.getEmployeeDirectory().createEmployee("Ibukun");
        UserAccount userAccount = system.getUserAccountDirectory().createUserAccount("superuser", "superuser", employeeSuper, new SuperUser());

        return system;
    }
}
