/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.Enterprise.Enterprise;
import Business.UserAccount.UserAccount;
import Business.Vaccine.Vaccine;
import java.util.Date;

/**
 *
 * @author mubarakibukunoluwa
 */
public  class WorkRequest {

    private String message;
    private Vaccine vaccine;
    private int amount;
    private Enterprise enterprise;
    private UserAccount clinicUserAccount;
    private Enterprise receivingEnterprise;
    private UserAccount receivingAccount;
    private String ReceiverStatus;
    private Enterprise distributorEnterprise;
    private UserAccount distributorAccount;
    private String Status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Vaccine getVaccine() {
        return vaccine;
    }

    public void setVaccine(Vaccine vaccine) {
        this.vaccine = vaccine;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Enterprise getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }

    public UserAccount getClinicUserAccount() {
        return clinicUserAccount;
    }

    public void setClinicUserAccount(UserAccount clinicUserAccount) {
        this.clinicUserAccount = clinicUserAccount;
    }

    public Enterprise getReceivingEnterprise() {
        return receivingEnterprise;
    }

    public void setReceivingEnterprise(Enterprise receivingEnterprise) {
        this.receivingEnterprise = receivingEnterprise;
    }

    public UserAccount getReceivingAccount() {
        return receivingAccount;
    }

    public void setReceivingAccount(UserAccount receivingAccount) {
        this.receivingAccount = receivingAccount;
    }

    public String getReceiverStatus() {
        return ReceiverStatus;
    }

    public void setReceiverStatus(String ReceiverStatus) {
        this.ReceiverStatus = ReceiverStatus;
    }

    public Enterprise getDistributorEnterprise() {
        return distributorEnterprise;
    }

    public void setDistributorEnterprise(Enterprise distributorEnterprise) {
        this.distributorEnterprise = distributorEnterprise;
    }

    public UserAccount getDistributorAccount() {
        return distributorAccount;
    }

    public void setDistributorAccount(UserAccount distributorAccount) {
        this.distributorAccount = distributorAccount;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }
    
   

    
}
