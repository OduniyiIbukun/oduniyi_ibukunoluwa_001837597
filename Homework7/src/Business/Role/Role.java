/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Ecosystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.State;
import Business.Network;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;




/**
 *
 * @author mubarakibukunoluwa
 */
public abstract class Role {
    
    public enum RoleType{
        Admin("Admin"){},
        Doctor("Doctor"){},
        LabAssistant("Lab Assistant"){},
        Procurement("Vaccine Procurement"){},
        SuperUser("Super Admin"){},
        OrderManagementRole("Order Management Role"){},
        InventoryManagement("Inventory Management"){};
        
        private String value;
        private RoleType(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
    
    public abstract JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization,
            Enterprise enterprise, State state, Network network, Ecosystem system);

    @Override
    public String toString() {
        return this.getClass().getName();
    }
    
    
}