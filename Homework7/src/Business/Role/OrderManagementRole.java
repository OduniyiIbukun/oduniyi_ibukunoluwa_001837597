/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Ecosystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.State;
import Business.Network;
import Business.Organization.Organization;
import Business.Organization.RegulationOrganization;
import Business.UserAccount.UserAccount;
import UserInterface.RegulationManagementWorkArea.OrderManagementWorkArea;
import javax.swing.JPanel;

/**
 *
 * @author mubarakibukunoluwa
 */
public class OrderManagementRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, 
            State state, Network network, Ecosystem system) {
        return new OrderManagementWorkArea(userProcessContainer, account, (RegulationOrganization)organization, state);
    }
    
}
