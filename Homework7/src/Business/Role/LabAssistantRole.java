/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Ecosystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.State;
import Business.Network;
import Business.Organization.Clinic;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import UserInterface.Clinic.ClinicWorkArea;
import javax.swing.JPanel;



/**
 *
 * @author mubarakibukunoluwa
 */
public class LabAssistantRole extends Role {

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, State state, Network network, Ecosystem system) {
    return new ClinicWorkArea(userProcessContainer, account, (Clinic)organization, state);
    }

    //@Override
    //public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Business business) {
      //  return new LabAssistantWorkAreaJPanel(userProcessContainer, account, organization, business);
    //}
    
}
